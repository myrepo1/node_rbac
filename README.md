# Flat RBAC Demonstration with Node.js and PostgreSQL

Welcome to the Node.js and PostgreSQL demonstration showcasing the implementation of Flat Role-Based Access Control (RBAC) in the backend. This repository highlights the simplicity and effectiveness of Flat RBAC for managing access control in applications.

## Table of Contents

- [Features](#features)
- [Prerequisites](#prerequisites)
- [Setup](#setup)

## Purpose

The primary purpose of this backend is to efficiently manage users and their roles. All permissions are stored in the database, while the authorization logic for specific resources is embedded in the code. Notably, public endpoints such as login remain permission-free.
For certain permission, a account could have permission restriction for certain sources. This configuration stored in session table.

## Features

- **User Management:** Create, update, and delete user accounts.
- **Role Management:** Define roles with specific permissions.
- **Flat RBAC Structure:** Implement access control based on user roles. While adhering to a flat RBAC model, certain resources have permissions determined by user identity.

## Prerequisites

Before you begin, ensure you have the following installed:

- Node.js (v12 or higher)
- PostgreSQL
- DbMate

## Setup

1. Clone the repository:

   ```bash
   git clone https://github.com/your-username/flat-rbac-demo.git

   ```

2. Navigate to the project directory:
   ```bash
   cd flat-rbac-demo
   ```
3. Install dependencies:
   ```bash
   yarn install
   ```
4. Run Docker image
   ```bash
   docker compose up --build
   ```
5. Copy .env.example to .env
   ```bash
   cp .env.example .env
   ```
6. Migrate database
   ```bash
   dbmate up
   ```
7. Run server
   ```bash
   yarn start
   ```
