import request from 'supertest';
import getApp from '../../../src/app';
import {
  attachRolesToAccount,
  cleanUpAllTest,
  clearAccount,
  clearRole,
  generateAccount,
  generateRole,
  setRolePermissionRestriction,
} from '../../../src/helper/utility_test';
import {
  checkCacheRoleExist,
  clearCacheRole,
} from '../../../src/service/permission';
import { login } from '../../../src/service/account';
import { SuccessProcessResponse } from '../../../src/type';
import { generateJWT, validateJWT } from '../../../src/helper/security';
import { getSingleAccountCache } from '../../../src/service/cache';
import {
  getSession,
  stopAllSession,
  updateSessionConfig,
} from '../../../src/service/session';
import pool from '../../../src/config/pool';
import { delay } from '../../../src/helper/general';

beforeAll(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
});

afterEach(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
});

afterAll(async () => {
  await cleanUpAllTest();
});

describe('Test Refresh', () => {
  const app = getApp();
  it('should return response with status 200 given successful refresh token with updated account with its role restriction', async () => {
    // Prepare initial data
    const account = await generateAccount();
    const roleId = await generateRole('more_admin', [
      'restricted_get_account_detail',
    ]);
    await attachRolesToAccount(account.id, [roleId]);
    await setRolePermissionRestriction(
      account.id,
      roleId,
      'restricted_get_account_detail',
      { ids: [account.id] }
    );
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    expect(loginResult.result).toBe(true);
    // Check current access token still has one role
    const decodedInitialAccessToken = validateJWT(loginResult.data.accessToken);
    expect(decodedInitialAccessToken).toEqual({
      id: account.id,
      type: 'accessToken',
      exp: expect.anything(),
      iat: expect.anything(),
      sessionId: expect.any(String),
      roles: [roleId],
    });

    // Check current cache account still has one role
    expect(getSingleAccountCache(account.id)).toEqual({
      roleIds: [roleId],
      permissionRestrictions: {
        restricted_get_account_detail: {
          ids: [account.id],
        },
      },
    });

    // Check session data
    const initialSessionData = await getSession(
      account.id,
      decodedInitialAccessToken.sessionId
    );
    expect(initialSessionData).toEqual({
      id: decodedInitialAccessToken.sessionId,
      role_config: {
        roleIds: [roleId],
        permissionRestrictions: {
          restricted_get_account_detail: {
            ids: [account.id],
          },
        },
      },
    });

    // Attach new role
    const secondRoleId = await generateRole('second_admin', [
      'attach_permission_to_role',
      'detach_permission_from_role',
    ]);
    expect(checkCacheRoleExist(secondRoleId)).toBe(false);
    await attachRolesToAccount(account.id, [secondRoleId]);
    await updateSessionConfig(account.id, pool);

    const res = await request(app)
      .post('/account/refresh')
      .send({ refreshToken: loginResult.data.refreshToken })
      .set('Accept', 'application/json');
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: {
        accessToken: expect.any(String),
        refreshToken: expect.any(String),
      },
    });
    // Check accessToken has correct payload
    expect(res.body.data.accessToken).not.toEqual(loginResult.data.accessToken);
    const decodedAccessToken = validateJWT(res.body.data.accessToken);
    expect(decodedAccessToken).toEqual({
      id: account.id,
      type: 'accessToken',
      exp: expect.anything(),
      iat: expect.anything(),
      sessionId: expect.any(String),
      roles: [roleId, secondRoleId],
    });
    expect(
      (decodedAccessToken.exp || 0) - (decodedAccessToken.iat || 0)
    ).toEqual(60 * 60);

    // Check refreshToken is the same
    expect(res.body.data.refreshToken).toEqual(loginResult.data.refreshToken);

    // Check is role cached
    expect(checkCacheRoleExist(roleId)).toBe(true);
    expect(checkCacheRoleExist(secondRoleId)).toBe(true);

    // Check is account cached
    expect(getSingleAccountCache(account.id)).toEqual({
      roleIds: [roleId, secondRoleId],
      permissionRestrictions: {
        restricted_get_account_detail: {
          ids: [account.id],
        },
      },
    });

    // Check session data
    const sessionData = await getSession(
      account.id,
      decodedAccessToken.sessionId
    );
    expect(sessionData).toEqual({
      id: decodedAccessToken.sessionId,
      role_config: {
        roleIds: [roleId, secondRoleId],
        permissionRestrictions: {
          restricted_get_account_detail: {
            ids: [account.id],
          },
        },
      },
    });
  });
  it('should return response with status 400 given wrong refresh token', async () => {
    const refreshToken = 'wrong';
    const res = await request(app)
      .post('/account/refresh')
      .send({ refreshToken })
      .set('Accept', 'application/json');
    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({
      result: false,
      message: 'Invalid refresh token',
    });
  });
  it('should return response with status 400 given expired refresh token', async () => {
    const expiredRefreshToken = generateJWT(
      { id: 'admin', type: 'refreshToken' },
      '10ms'
    );
    await delay(10);
    const res = await request(app)
      .post('/account/refresh')
      .send({ refreshToken: expiredRefreshToken })
      .set('Accept', 'application/json');
    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({
      result: false,
      message: 'Invalid refresh token',
    });
  });
  it('should return response with status 400 given active session not found', async () => {
    // Prepare initial data
    const account = await generateAccount();
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    await stopAllSession(account.id);
    const res = await request(app)
      .post('/account/refresh')
      .send({ refreshToken: loginResult.data.refreshToken })
      .set('Accept', 'application/json');
    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({
      result: false,
      message: 'Invalid refresh token',
    });
  });
});
