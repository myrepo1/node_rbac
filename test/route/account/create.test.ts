import request from 'supertest';
import getApp from '../../../src/app';
import { generateDummyAccessToken } from '../../../src/helper/security';
import { clearCacheRole } from '../../../src/service/permission';
import {
  attachRolesToAccount,
  cleanUpAllTest,
  clearAccount,
  clearRole,
  generateAccount,
  generateRole,
} from '../../../src/helper/utility_test';
import { resetAccountData } from '../../../src/service/cache';
import { login } from '../../../src/service/account';
import { SuccessProcessResponse } from '../../../src/type';
import { faker } from '@faker-js/faker';
import pool from '../../../src/config/pool';

beforeAll(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
});

afterEach(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
});

afterAll(async () => {
  await cleanUpAllTest();
});

describe('Test Create Account', () => {
  const app = getApp();
  it('should return response with status 201 given successful create account', async () => {
    // Prepare initial data
    const account = await generateAccount();
    const roleId = await generateRole('account_creator', ['create_account']);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = {
      email: faker.internet.email(),
      password: faker.internet.password(),
      name: 'dummy_account',
    };
    const res = await request(app)
      .post('/account/create')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(201);
    expect(res.body).toEqual({
      result: true,
      data: {
        id: expect.any(Number),
        email: payload.email,
        name: payload.name,
      },
    });
    const newAccountQuery = `SELECT email FROM account WHERE id = $1 and email = $2`;
    const newAccountQueryResult = await pool.query<{ email: string }>(
      newAccountQuery,
      [res.body.data.id, payload.email]
    );
    expect(newAccountQueryResult.rows.length).toEqual(1);
  });
  it('should return response with status 201 cache session not exist when create account', async () => {
    // Prepare initial data
    const account = await generateAccount();
    const roleId = await generateRole('account_creator', ['create_account']);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    resetAccountData();
    clearCacheRole();
    const payload = {
      email: faker.internet.email(),
      password: faker.internet.password(),
      name: 'dummy_account',
    };
    const res = await request(app)
      .post('/account/create')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(201);
    expect(res.body).toEqual({
      result: true,
      data: {
        id: expect.any(Number),
        email: payload.email,
        name: payload.name,
      },
    });
    const newAccountQuery = `SELECT email FROM account WHERE id = $1 and email = $2`;
    const newAccountQueryResult = await pool.query<{ email: string }>(
      newAccountQuery,
      [res.body.data.id, payload.email]
    );
    expect(newAccountQueryResult.rows.length).toEqual(1);
  });
  it('should return response with status 201 given success create account with its roles', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('account_creator', [
      'create_account',
      'attach_role_to_account',
    ]);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const attachedRoleId = await generateRole('more_admin', [
      'get_account_detail',
    ]);
    const payload = {
      email: faker.internet.email(),
      password: faker.internet.password(),
      name: faker.person.firstName(),
      attachRoles: [attachedRoleId],
    };
    const res = await request(app)
      .post('/account/create')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(201);
    expect(res.body).toEqual({
      result: true,
      data: {
        id: expect.any(Number),
        email: payload.email,
        name: payload.name,
      },
    });
    const newAccountQuery = `SELECT 
            email, 
            (SELECT jsonb_agg(role_id) FROM (
              SELECT role_id
              FROM account_role
              WHERE account_role.account_id = account.id
          ) s) AS role_data 
          FROM account WHERE id = $1 and email = $2`;
    const newAccountQueryResult = await pool.query<{
      email: string;
      role_data: number[];
    }>(newAccountQuery, [res.body.data.id, payload.email]);
    expect(newAccountQueryResult.rows.length).toEqual(1);
    expect(newAccountQueryResult.rows[0].role_data).toEqual([attachedRoleId]);
  });
  it('should return response with status 401 given account not exist in cache and db when create account', async () => {
    const payload = {
      email: faker.internet.email(),
      password: faker.internet.password(),
      name: 'dummy_account',
    };
    const accessToken = generateDummyAccessToken();
    const res = await request(app)
      .post('/account/create')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${accessToken}`);
    expect(res.statusCode).toEqual(401);
    const newAccountQuery = `SELECT email FROM account WHERE email = $1`;
    const newAccountQueryResult = await pool.query<{ email: string }>(
      newAccountQuery,
      [payload.email]
    );
    expect(newAccountQueryResult.rows.length).toEqual(0);
  });
  it('should get response with status 401 given request without access token', async () => {
    const payload = { email: 'admin', password: 'admin', name: 'admin' };
    const res = await request(app)
      .post('/account/create')
      .send(payload)
      .set('Accept', 'application/json');
    expect(res.statusCode).toEqual(401);
    expect(res.body).toEqual({
      result: false,
      message: 'Missing access token',
    });
    const newAccountQuery = `SELECT email FROM account WHERE email = $1`;
    const newAccountQueryResult = await pool.query<{ email: string }>(
      newAccountQuery,
      [payload.email]
    );
    expect(newAccountQueryResult.rows.length).toEqual(0);
  });
  it('should get response with status 403 given invalid permissions', async () => {
    // Prepare initial data
    const account = await generateAccount();
    const roleId = await generateRole('not_account_creator', ['list_account']);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = {
      email: faker.internet.email(),
      password: faker.internet.password(),
      name: 'dummy_account',
    };
    const res = await request(app)
      .post('/account/create')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(403);
    expect(res.body).toEqual({ result: false, message: 'Invalid permissions' });
    const newAccountQuery = `SELECT email FROM account WHERE email = $1`;
    const newAccountQueryResult = await pool.query<{ email: string }>(
      newAccountQuery,
      [payload.email]
    );
    expect(newAccountQueryResult.rows.length).toEqual(0);
  });
  it('should return response with status 400 given invalid account data', async () => {
    // Prepare initial data
    const account = await generateAccount();
    const roleId = await generateRole('account_creator', ['create_account']);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = {
      email: faker.internet.email(),
      password: '',
      name: 'dummy_account',
    };
    const res = await request(app)
      .post('/account/create')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({
      result: false,
      message: 'Invalid payload',
    });
    const newAccountQuery = `SELECT email FROM account WHERE email = $1`;
    const newAccountQueryResult = await pool.query<{ email: string }>(
      newAccountQuery,
      [payload.email]
    );
    expect(newAccountQueryResult.rows.length).toEqual(0);
  });
  it('should return response with status 400 given email already exist', async () => {
    // Prepare initial data
    const account = await generateAccount();
    const roleId = await generateRole('account_creator', ['create_account']);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = {
      email: account.email,
      password: faker.internet.password(),
      name: 'dummy_account',
    };
    const res = await request(app)
      .post('/account/create')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({
      result: false,
      message: 'Email already exists or invalid data',
    });
    const newAccountQuery = `SELECT email FROM account WHERE email = $1`;
    const newAccountQueryResult = await pool.query<{ email: string }>(
      newAccountQuery,
      [payload.email]
    );
    expect(newAccountQueryResult.rows.length).toEqual(1);
  });
  it('should return response with status 400 given attached role not exist', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('account_creator', [
      'create_account',
      'attach_role_to_account',
    ]);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = {
      email: faker.internet.email(),
      password: faker.internet.password(),
      name: faker.person.firstName(),
      attachRoles: [100],
    };
    const res = await request(app)
      .post('/account/create')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({
      result: false,
      message: 'Email already exists or invalid data',
    });
    const newAccountQuery = `SELECT email FROM account WHERE email = $1`;
    const newAccountQueryResult = await pool.query<{ email: string }>(
      newAccountQuery,
      [payload.email]
    );
    expect(newAccountQueryResult.rows.length).toEqual(0);
  });
});
