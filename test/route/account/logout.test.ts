import request from 'supertest';
import getApp from '../../../src/app';
import {
  attachRolesToAccount,
  cleanUpAllTest,
  clearAccount,
  clearRole,
  generateAccount,
  generateRole,
  setRolePermissionRestriction,
} from '../../../src/helper/utility_test';
import { validateJWT } from '../../../src/helper/security';
import {
  checkCacheRoleExist,
  clearCacheRole,
} from '../../../src/service/permission';
import { getSession } from '../../../src/service/session';
import { getSingleAccountCache } from '../../../src/service/cache';
import { SuccessProcessResponse } from '../../../src/type';
import { checkCacheAccountExist, login } from '../../../src/service/account';

beforeAll(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
});

afterEach(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
});

afterAll(async () => {
  await cleanUpAllTest();
});

describe('Test Logout', () => {
  const app = getApp();
  it('should return response with status 200 given successful logout with account with role restriction', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('more_admin', [
      'restricted_get_account_detail',
    ]);
    await attachRolesToAccount(account.id, [roleId]);
    await setRolePermissionRestriction(
      account.id,
      roleId,
      'restricted_get_account_detail',
      { ids: [account.id] }
    );
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;

    const decodedAccessToken = validateJWT(loginResult.data.accessToken);

    // Check is role cached
    expect(checkCacheRoleExist(roleId)).toBe(true);

    // Check is account cached
    const cacheAccount = getSingleAccountCache(account.id);
    expect(cacheAccount).toEqual({
      roleIds: [roleId],
      permissionRestrictions: {
        restricted_get_account_detail: {
          ids: [account.id],
        },
      },
    });

    // Check session data
    const sessionData = await getSession(
      account.id,
      decodedAccessToken.sessionId
    );
    expect(sessionData).toEqual({
      id: decodedAccessToken.sessionId,
      role_config: {
        roleIds: [roleId],
        permissionRestrictions: {
          restricted_get_account_detail: {
            ids: [account.id],
          },
        },
      },
    });
    const res = await request(app)
      .post('/account/logout')
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: null,
    });
    const updatedSession = await getSession(
      account.id,
      decodedAccessToken.sessionId
    );
    expect(updatedSession).toBeUndefined();
    expect(checkCacheAccountExist(account.id)).toEqual(false);
  });
});
