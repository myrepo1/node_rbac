import request from 'supertest';
import getApp from '../../../src/app';
import { clearCacheRole } from '../../../src/service/permission';
import {
  attachRolesToAccount,
  cleanUpAllTest,
  clearAccount,
  clearRole,
  generateAccount,
  generateRole,
  setRolePermissionRestriction,
} from '../../../src/helper/utility_test';
import { resetAccountData } from '../../../src/service/cache';
import { login } from '../../../src/service/account';
import { SuccessProcessResponse } from '../../../src/type';

beforeAll(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
});

afterEach(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
  resetAccountData();
});

afterAll(async () => {
  await cleanUpAllTest();
});

describe('Test Get Single Account', () => {
  const app = getApp();
  it('should return response with status 200 given successful get single account with get_account_detail permission', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('account_getter', ['get_account_detail']);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const res = await request(app)
      .get(`/account/account/${account.id}`)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: {
        id: expect.any(Number),
        email: account.email,
        name: account.name,
        roles: [
          {
            role: 'account_getter',
            permissions: ['get_account_detail'],
          },
        ],
      },
    });
  });
  it('should return response with status 200 given successful get single account with restricted_get_account_detail permission and match restriction', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('account_getter', [
      'restricted_get_account_detail',
    ]);
    await attachRolesToAccount(account.id, [roleId]);
    await setRolePermissionRestriction(
      account.id,
      roleId,
      'restricted_get_account_detail',
      { ids: [account.id] }
    );
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const res = await request(app)
      .get(`/account/account/${account.id}`)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: {
        id: expect.any(Number),
        email: account.email,
        name: account.name,
        roles: [
          {
            role: 'account_getter',
            permissions: ['restricted_get_account_detail'],
          },
        ],
      },
    });
  });
  it('should return response with status 404 given failed get single account with restricted_get_account_detail permission and not match restriction', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('account_getter', [
      'restricted_get_account_detail',
    ]);
    await attachRolesToAccount(account.id, [roleId]);
    await setRolePermissionRestriction(
      account.id,
      roleId,
      'restricted_get_account_detail',
      { ids: [account.id] }
    );
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const res = await request(app)
      .get(`/account/account/${account.id + 1}`)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(404);
    expect(res.body).toEqual({
      result: false,
      message: 'Account not found',
    });
  });
  it('should return response with status 404 given failed get single account with restricted_get_account_detail permission and wrong restriction schema', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('account_getter', [
      'restricted_get_account_detail',
    ]);
    await attachRolesToAccount(account.id, [roleId]);
    await setRolePermissionRestriction(
      account.id,
      roleId,
      'restricted_get_account_detail',
      { account_ids: [account.id] }
    );
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const res = await request(app)
      .get(`/account/account/${account.id}`)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(404);
    expect(res.body).toEqual({
      result: false,
      message: 'Account not found',
    });
  });
  it('should get response with status 401 given invalid access token', async () => {
    const accessToken = 'invalid';
    const res = await request(app)
      .get('/account/account/1')
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${accessToken}`);
    expect(res.statusCode).toEqual(401);
    expect(res.body).toEqual({
      result: false,
      message: 'Invalid access token',
    });
  });
  it('should get response with status 401 given request without access token', async () => {
    const res = await request(app)
      .get('/account/account/1')
      .set('Accept', 'application/json');
    expect(res.statusCode).toEqual(401);
    expect(res.body).toEqual({
      result: false,
      message: 'Missing access token',
    });
  });
  it('should get response with status 403 given invalid permissions', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('account_getter', ['delete_account']);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const res = await request(app)
      .get(`/account/account/${account.id}`)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(403);
    expect(res.body).toEqual({ result: false, message: 'Invalid permissions' });
  });
});
