import request from 'supertest';
import getApp from '../../../src/app';
import { validateJWT } from '../../../src/helper/security';
import { clearCacheRole } from '../../../src/service/permission';
import {
  attachRolesToAccount,
  cleanUpAllTest,
  clearAccount,
  clearRole,
  generateAccount,
  generateRole,
  setRolePermissionRestriction,
} from '../../../src/helper/utility_test';
import {
  getSingleAccountCache,
  resetAccountData,
} from '../../../src/service/cache';
import { login } from '../../../src/service/account';
import { SuccessProcessResponse } from '../../../src/type';
import pool from '../../../src/config/pool';
import { getSession } from '../../../src/service/session';

beforeAll(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
});

afterEach(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
  resetAccountData();
});

afterAll(async () => {
  await cleanUpAllTest();
});

describe('Test Update Single Account', () => {
  const app = getApp();
  it('should return response with status 200 given successful update single account with update_account permission', async () => {
    const account = await generateAccount();
    const targetAccount = await generateAccount();
    const roleId = await generateRole('account_updater', ['update_account']);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = { name: 'updated' };
    const res = await request(app)
      .put(`/account/account/${targetAccount.id}`)
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: {
        id: targetAccount.id,
        name: 'updated',
        email: targetAccount.email,
      },
    });
    const updatedAccountQuery = `SELECT name FROM account WHERE id = $1`;
    const updatedAccountQueryResult = await pool.query<{ name: string }>(
      updatedAccountQuery,
      [targetAccount.id]
    );
    expect(updatedAccountQueryResult.rows[0].name).toEqual(payload.name);
  });
  it('should return response with status 200 given successful update single account with attach_role_to_account and update_account permission', async () => {
    const account = await generateAccount();
    const targetAccount = await generateAccount();
    const roleId = await generateRole('account_updater', [
      'update_account',
      'attach_role_to_account',
    ]);
    const newRoleId = await generateRole('account_creator', ['create_account']);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const targetLoginResult = (await login({
      email: targetAccount.email,
      password: targetAccount.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const decodedInitialAccessToken = validateJWT(
      targetLoginResult.data.accessToken
    );
    const currentTargetSession = await getSession(
      targetAccount.id,
      decodedInitialAccessToken.sessionId
    );
    expect(currentTargetSession).toEqual({
      id: decodedInitialAccessToken.sessionId,
      role_config: {
        roleIds: [],
        permissionRestrictions: {},
      },
    });
    const cacheAccount = getSingleAccountCache(targetAccount.id);
    expect(cacheAccount).toEqual({
      roleIds: [],
      permissionRestrictions: {},
    });
    const payload = { attachRoles: [newRoleId] };
    const res = await request(app)
      .put(`/account/account/${targetAccount.id}`)
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: null,
    });
    const updatedAccountQuery = `SELECT 
            name,
            (SELECT jsonb_agg(role_id) FROM (
            SELECT role_id
            FROM account_role
            WHERE account_role.account_id = account.id
            ) s) AS role_data 
          FROM account WHERE id = $1`;
    const updateAccountQueryResult = await pool.query<{
      name: string;
      role_data: number[];
    }>(updatedAccountQuery, [targetAccount.id]);
    expect(updateAccountQueryResult.rows.length).toEqual(1);
    expect(updateAccountQueryResult.rows[0].role_data).toEqual([newRoleId]);
    const updatedTargetSession = await getSession(
      targetAccount.id,
      decodedInitialAccessToken.sessionId
    );
    expect(updatedTargetSession).toEqual({
      id: decodedInitialAccessToken.sessionId,
      role_config: {
        roleIds: [newRoleId],
        permissionRestrictions: {},
      },
    });
    const updatedCacheAccount = getSingleAccountCache(targetAccount.id);
    expect(updatedCacheAccount).toEqual({
      roleIds: [newRoleId],
      permissionRestrictions: {},
    });
  });
  it('should return response with status 200 given successful update single account with detach_role_to_account and update_account permission', async () => {
    const account = await generateAccount();
    const targetAccount = await generateAccount();
    const roleId = await generateRole('account_updater', [
      'update_account',
      'detach_role_from_account',
    ]);
    const newRoleId = await generateRole('account_creator', ['create_account']);
    await attachRolesToAccount(account.id, [roleId]);
    await attachRolesToAccount(targetAccount.id, [newRoleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const targetLoginResult = (await login({
      email: targetAccount.email,
      password: targetAccount.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const decodedInitialAccessToken = validateJWT(
      targetLoginResult.data.accessToken
    );
    const currentTargetSession = await getSession(
      targetAccount.id,
      decodedInitialAccessToken.sessionId
    );
    expect(currentTargetSession).toEqual({
      id: decodedInitialAccessToken.sessionId,
      role_config: {
        roleIds: [newRoleId],
        permissionRestrictions: {},
      },
    });
    const cacheAccount = getSingleAccountCache(targetAccount.id);
    expect(cacheAccount).toEqual({
      roleIds: [newRoleId],
      permissionRestrictions: {},
    });
    const payload = { detachRoles: [newRoleId] };
    const res = await request(app)
      .put(`/account/account/${targetAccount.id}`)
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: null,
    });
    const updatedAccountQuery = `SELECT 
          name,
          (SELECT jsonb_agg(role_id) FROM (
          SELECT role_id
          FROM account_role
          WHERE account_role.account_id = account.id
          ) s) AS role_data 
        FROM account WHERE id = $1`;
    const updateAccountQueryResult = await pool.query<{
      name: string;
      role_data: number[];
    }>(updatedAccountQuery, [targetAccount.id]);
    expect(updateAccountQueryResult.rows.length).toEqual(1);
    expect(updateAccountQueryResult.rows[0].role_data).toEqual(null);
    const updatedTargetSession = await getSession(
      targetAccount.id,
      decodedInitialAccessToken.sessionId
    );
    expect(updatedTargetSession).toEqual({
      id: decodedInitialAccessToken.sessionId,
      role_config: {
        roleIds: [],
        permissionRestrictions: {},
      },
    });
    const updatedCacheAccount = getSingleAccountCache(targetAccount.id);
    expect(updatedCacheAccount).toEqual({
      roleIds: [],
      permissionRestrictions: {},
    });
  });
  it('should return response with status 200 given successful update single account with update_account,attach_role_to_account,detach_role_from_account permission', async () => {
    const account = await generateAccount();
    const targetAccount = await generateAccount();
    const roleId = await generateRole('account_updater', [
      'update_account',
      'attach_role_to_account',
      'detach_role_from_account',
    ]);
    const currentRoleId = await generateRole('account_lister', [
      'list_account',
    ]);
    const currentSecondRoleId = await generateRole('account_deleter', [
      'delete_account',
    ]);
    const newRoleId = await generateRole('account_creator', ['create_account']);
    await attachRolesToAccount(account.id, [roleId]);
    await attachRolesToAccount(targetAccount.id, [
      currentRoleId,
      currentSecondRoleId,
    ]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const targetLoginResult = (await login({
      email: targetAccount.email,
      password: targetAccount.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const decodedInitialAccessToken = validateJWT(
      targetLoginResult.data.accessToken
    );
    const currentTargetSession = await getSession(
      targetAccount.id,
      decodedInitialAccessToken.sessionId
    );
    expect(currentTargetSession).toEqual({
      id: decodedInitialAccessToken.sessionId,
      role_config: {
        roleIds: [currentRoleId, currentSecondRoleId],
        permissionRestrictions: {},
      },
    });
    const cacheAccount = getSingleAccountCache(targetAccount.id);
    expect(cacheAccount).toEqual({
      roleIds: [currentRoleId, currentSecondRoleId],
      permissionRestrictions: {},
    });
    const payload = {
      name: 'updated',
      attachRoles: [newRoleId],
      detachRoles: [currentSecondRoleId],
    };
    const res = await request(app)
      .put(`/account/account/${targetAccount.id}`)
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: {
        id: targetAccount.id,
        name: 'updated',
        email: targetAccount.email,
      },
    });
    const updatedAccountQuery = `SELECT 
        name,
        (SELECT jsonb_agg(role_id) FROM (
        SELECT role_id
        FROM account_role
        WHERE account_role.account_id = account.id
        ) s) AS role_data 
      FROM account WHERE id = $1`;
    const updateAccountQueryResult = await pool.query<{
      name: string;
      role_data: number[];
    }>(updatedAccountQuery, [targetAccount.id]);
    expect(updateAccountQueryResult.rows.length).toEqual(1);
    expect(updateAccountQueryResult.rows[0].name).toEqual('updated');
    expect(updateAccountQueryResult.rows[0].role_data).toEqual([
      currentRoleId,
      newRoleId,
    ]);
    const updatedTargetSession = await getSession(
      targetAccount.id,
      decodedInitialAccessToken.sessionId
    );
    expect(updatedTargetSession).toEqual({
      id: decodedInitialAccessToken.sessionId,
      role_config: {
        roleIds: [currentRoleId, newRoleId],
        permissionRestrictions: {},
      },
    });
    const updatedCacheAccount = getSingleAccountCache(targetAccount.id);
    expect(updatedCacheAccount).toEqual({
      roleIds: [currentRoleId, newRoleId],
      permissionRestrictions: {},
    });
  });
  it('should return response with status 200 given successful update single account with restricted_update_account permission', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('account_updater', [
      'restricted_update_account',
    ]);
    await attachRolesToAccount(account.id, [roleId]);
    await setRolePermissionRestriction(
      account.id,
      roleId,
      'restricted_update_account',
      { ids: [account.id] }
    );
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = { name: 'updated' };
    const res = await request(app)
      .put(`/account/account/${account.id}`)
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: {
        id: account.id,
        name: 'updated',
        email: account.email,
      },
    });
    const updatedAccountQuery = `SELECT name FROM account WHERE id = $1`;
    const updatedAccountQueryResult = await pool.query<{ name: string }>(
      updatedAccountQuery,
      [account.id]
    );
    expect(updatedAccountQueryResult.rows[0].name).toEqual(payload.name);
  });
  it('should return response with status 400 given update_account permission but wrong payload', async () => {
    const account = await generateAccount();
    const targetAccount = await generateAccount();
    const roleId = await generateRole('account_updater', ['update_account']);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = { name: '' };
    const res = await request(app)
      .put(`/account/account/${targetAccount.id}`)
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({
      result: false,
      message: 'Invalid payload',
    });
    const updatedAccountQuery = `SELECT name FROM account WHERE id = $1`;
    const updatedAccountQueryResult = await pool.query<{ name: string }>(
      updatedAccountQuery,
      [targetAccount.id]
    );
    expect(updatedAccountQueryResult.rows[0].name).toEqual(targetAccount.name);
  });
  it('should return response with status 400 given update_account permission but account not found', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('account_updater', ['update_account']);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = { name: 'updated' };
    const res = await request(app)
      .put(`/account/account/${account.id + 1}`)
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({
      result: false,
      message: 'Account not found',
    });
  });
  it('should return response with status 400 given attach_role_to_account and update_account permission but role not found', async () => {
    const account = await generateAccount();
    const targetAccount = await generateAccount();
    const roleId = await generateRole('account_updater', [
      'update_account',
      'attach_role_to_account',
    ]);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = { attachRoles: [roleId + 1] };
    const res = await request(app)
      .put(`/account/account/${targetAccount.id}`)
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({
      result: false,
      message: expect.any(String),
    });
  });
  it('should return response with status 400 given update single account with restricted_update_account permission but restriction not match', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('account_updater', [
      'restricted_update_account',
    ]);
    await attachRolesToAccount(account.id, [roleId]);
    await setRolePermissionRestriction(
      account.id,
      roleId,
      'restricted_update_account',
      { ids: [account.id + 1] }
    );
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = { name: 'updated' };
    const res = await request(app)
      .put(`/account/account/${account.id}`)
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({
      result: false,
      message: 'Account not found',
    });
    const updatedAccountQuery = `SELECT name FROM account WHERE id = $1`;
    const updatedAccountQueryResult = await pool.query<{ name: string }>(
      updatedAccountQuery,
      [account.id]
    );
    expect(updatedAccountQueryResult.rows[0].name).toEqual(account.name);
  });
  it('should get response with status 401 given invalid access token', async () => {
    const payload = { name: 'updated' };
    const accessToken = 'invalid';
    const res = await request(app)
      .put('/account/account/1')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${accessToken}`);
    expect(res.statusCode).toEqual(401);
    expect(res.body).toEqual({
      result: false,
      message: 'Invalid access token',
    });
  });
  it('should get response with status 401 given request without access token', async () => {
    const payload = { name: 'updated' };
    const res = await request(app)
      .put('/account/account/1')
      .send(payload)
      .set('Accept', 'application/json');
    expect(res.statusCode).toEqual(401);
    expect(res.body).toEqual({
      result: false,
      message: 'Missing access token',
    });
  });
  it('should get response with status 403 given invalid permissions', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('account_updater', ['create_account']);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = { name: 'updated' };
    const res = await request(app)
      .put(`/account/account/${account.id}`)
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(403);
    expect(res.body).toEqual({ result: false, message: 'Invalid permissions' });
  });
});
