import request from 'supertest';
import getApp from '../../../src/app';
import {
  attachRolesToAccount,
  cleanUpAllTest,
  clearAccount,
  clearRole,
  generateAccount,
  generateRole,
  setRolePermissionRestriction,
} from '../../../src/helper/utility_test';
import { validateJWT } from '../../../src/helper/security';
import {
  checkCacheRoleExist,
  clearCacheRole,
} from '../../../src/service/permission';
import { getSession } from '../../../src/service/session';
import { getSingleAccountCache } from '../../../src/service/cache';

beforeAll(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
});

afterEach(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
});

afterAll(async () => {
  await cleanUpAllTest();
});

describe('Test Login', () => {
  const app = getApp();
  it('should return response with status 200 given successful login with account with role restriction', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('more_admin', [
      'restricted_get_account_detail',
    ]);
    await attachRolesToAccount(account.id, [roleId]);
    await setRolePermissionRestriction(
      account.id,
      roleId,
      'restricted_get_account_detail',
      { ids: [account.id] }
    );
    const payload = { email: account.email, password: account.password };
    const res = await request(app)
      .post('/account/login')
      .send(payload)
      .set('Accept', 'application/json');
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: {
        accessToken: expect.any(String),
        refreshToken: expect.any(String),
      },
    });
    // Check accessToken has correct payload
    const decodedAccessToken = validateJWT(res.body.data.accessToken);
    expect(decodedAccessToken).toEqual({
      id: account.id,
      type: 'accessToken',
      exp: expect.anything(),
      iat: expect.anything(),
      sessionId: expect.any(String),
      roles: [roleId],
    });
    expect(
      (decodedAccessToken.exp || 0) - (decodedAccessToken.iat || 0)
    ).toEqual(60 * 60);

    // Check refreshToken has correct payload
    const decodedRefreshToken = validateJWT(res.body.data.refreshToken);
    expect(decodedRefreshToken).toEqual({
      id: account.id,
      type: 'refreshToken',
      exp: expect.anything(),
      iat: expect.anything(),
      sessionId: expect.any(String),
    });
    expect(
      (decodedRefreshToken.exp || 0) - (decodedRefreshToken.iat || 0)
    ).toEqual(60 * 60 * 24 * 7);

    // Check is role cached
    expect(checkCacheRoleExist(roleId)).toBe(true);

    // Check is account cached
    const cacheAccount = getSingleAccountCache(account.id);
    expect(cacheAccount).toEqual({
      roleIds: [roleId],
      permissionRestrictions: {
        restricted_get_account_detail: {
          ids: [account.id],
        },
      },
    });

    // Check session data
    const sessionData = await getSession(
      account.id,
      decodedAccessToken.sessionId
    );
    expect(sessionData).toEqual({
      id: decodedAccessToken.sessionId,
      role_config: {
        roleIds: [roleId],
        permissionRestrictions: {
          restricted_get_account_detail: {
            ids: [account.id],
          },
        },
      },
    });
  });
  it('should return response with status 200 given successful login with account without role restriction', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('more_admin', ['get_account_detail']);
    await attachRolesToAccount(account.id, [roleId]);
    const payload = { email: account.email, password: account.password };
    const res = await request(app)
      .post('/account/login')
      .send(payload)
      .set('Accept', 'application/json');
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: {
        accessToken: expect.any(String),
        refreshToken: expect.any(String),
      },
    });
    // Check accessToken has correct payload
    const decodedAccessToken = validateJWT(res.body.data.accessToken);
    expect(decodedAccessToken).toEqual({
      id: account.id,
      type: 'accessToken',
      exp: expect.anything(),
      iat: expect.anything(),
      sessionId: expect.any(String),
      roles: [roleId],
    });
    expect(
      (decodedAccessToken.exp || 0) - (decodedAccessToken.iat || 0)
    ).toEqual(60 * 60);

    // Check refreshToken has correct payload
    const decodedRefreshToken = validateJWT(res.body.data.refreshToken);
    expect(decodedRefreshToken).toEqual({
      id: account.id,
      type: 'refreshToken',
      exp: expect.anything(),
      iat: expect.anything(),
      sessionId: expect.any(String),
    });
    expect(
      (decodedRefreshToken.exp || 0) - (decodedRefreshToken.iat || 0)
    ).toEqual(60 * 60 * 24 * 7);

    // Check is role cached
    expect(checkCacheRoleExist(roleId)).toBe(true);

    // Check is account cached
    const cacheAccount = getSingleAccountCache(account.id);
    expect(cacheAccount).toEqual({
      roleIds: [roleId],
      permissionRestrictions: {},
    });

    // Check session data
    const sessionData = await getSession(
      account.id,
      decodedAccessToken.sessionId
    );
    expect(sessionData).toEqual({
      id: decodedAccessToken.sessionId,
      role_config: {
        roleIds: [roleId],
        permissionRestrictions: {},
      },
    });
  });
  it('should return response with status 200 given successful login with account without role attached', async () => {
    const account = await generateAccount();
    const payload = { email: account.email, password: account.password };
    const res = await request(app)
      .post('/account/login')
      .send(payload)
      .set('Accept', 'application/json');
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: {
        accessToken: expect.any(String),
        refreshToken: expect.any(String),
      },
    });
    // Check accessToken has correct payload
    const decodedAccessToken = validateJWT(res.body.data.accessToken);
    expect(decodedAccessToken).toEqual({
      id: account.id,
      type: 'accessToken',
      exp: expect.anything(),
      iat: expect.anything(),
      sessionId: expect.any(String),
      roles: [],
    });
    expect(
      (decodedAccessToken.exp || 0) - (decodedAccessToken.iat || 0)
    ).toEqual(60 * 60);

    // Check refreshToken has correct payload
    const decodedRefreshToken = validateJWT(res.body.data.refreshToken);
    expect(decodedRefreshToken).toEqual({
      id: account.id,
      type: 'refreshToken',
      exp: expect.anything(),
      iat: expect.anything(),
      sessionId: expect.any(String),
    });
    expect(
      (decodedRefreshToken.exp || 0) - (decodedRefreshToken.iat || 0)
    ).toEqual(60 * 60 * 24 * 7);

    // Check is account cached
    const cacheAccount = getSingleAccountCache(account.id);
    expect(cacheAccount).toEqual({
      roleIds: [],
      permissionRestrictions: {},
    });

    // Check session data
    const sessionData = await getSession(
      account.id,
      decodedAccessToken.sessionId
    );
    expect(sessionData).toEqual({
      id: decodedAccessToken.sessionId,
      role_config: {
        roleIds: [],
        permissionRestrictions: {},
      },
    });
  });
  it('should get response with status 400 given wrong password', async () => {
    const account = await generateAccount();
    const payload = { email: account.email, password: account.password + '1' };
    const res = await request(app)
      .post('/account/login')
      .send(payload)
      .set('Accept', 'application/json');
    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({
      result: false,
      message: 'Invalid email or password',
    });
  });
  it('should get response with status 400 given empty email', async () => {
    const payload = { email: '', password: 'pass' };
    const res = await request(app)
      .post('/account/login')
      .send(payload)
      .set('Accept', 'application/json');
    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({
      result: false,
      message: 'Invalid email or password',
    });
  });
});
