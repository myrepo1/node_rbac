import request from 'supertest';
<<<<<<< Updated upstream
import getApp from '../../../src/app';
import { generateDummyAccessToken } from '../../../src/helper/security';
import { clearCacheRole } from '../../../src/service/permission';
import {
  attachRolesToAccount,
  cacheAccountWithoutDb,
  cacheRoleWithoutDb,
  cleanUpAllTest,
  clearAccount,
  clearRole,
  generateAccount,
  generateRole,
  setRolePermissionRestriction,
} from '../../../src/helper/utility_test';
=======
import getApp from "../../../src/app"
import { clearCacheRole } from '../../../src/service/permission';
import { attachRolesToAccount, cleanUpAllTest, clearAccount, clearRole, generateAccount, generateRole, setRolePermissionRestriction } from '../../../src/helper/utility_test';
>>>>>>> Stashed changes
import { resetAccountData } from '../../../src/service/cache';
import { checkCacheAccountExist, login } from '../../../src/service/account';
import { SuccessProcessResponse } from '../../../src/type';
import pool from '../../../src/config/pool';

beforeAll(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
});

afterEach(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
  resetAccountData();
});

afterAll(async () => {
  await cleanUpAllTest();
});

describe('Test Delete Single Account', () => {
  const app = getApp();
  it('should return response with status 200 given successful delete single account with delete_account permission', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('account_deleter', ['delete_account']);
    await attachRolesToAccount(account.id, [roleId]);
    const targetAccount = await generateAccount();
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    await login({
      email: targetAccount.email,
      password: targetAccount.password,
    });
    expect(checkCacheAccountExist(targetAccount.id)).toEqual(true);
    const res = await request(app)
      .delete(`/account/account/${targetAccount.id}`)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: null,
    });
    const newAccountQuery = `SELECT id FROM account WHERE id = $1`;
    const newAccountQueryResult = await pool.query<{ id: number }>(
      newAccountQuery,
      [targetAccount.id]
    );
    expect(newAccountQueryResult.rows.length).toEqual(0);
    expect(checkCacheAccountExist(targetAccount.id)).toBe(false);
  });
  it('should return response with status 200 given successful delete single account with restricted_delete_account permission', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('account_deleter', [
      'restricted_delete_account',
    ]);
    await attachRolesToAccount(account.id, [roleId]);
    const targetAccount = await generateAccount();
    await setRolePermissionRestriction(
      account.id,
      roleId,
      'restricted_delete_account',
      { ids: [targetAccount.id] }
    );
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    await login({
      email: targetAccount.email,
      password: targetAccount.password,
    });
    expect(checkCacheAccountExist(targetAccount.id)).toEqual(true);
    const res = await request(app)
      .delete(`/account/account/${targetAccount.id}`)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: null,
    });
    const newAccountQuery = `SELECT id FROM account WHERE id = $1`;
    const newAccountQueryResult = await pool.query<{ id: number }>(
      newAccountQuery,
      [targetAccount.id]
    );
    expect(newAccountQueryResult.rows.length).toEqual(0);
    expect(checkCacheAccountExist(targetAccount.id)).toBe(false);
  });
  it('should return response with status 400 given account not found', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('account_delete', ['delete_account']);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const res = await request(app)
      .delete(`/account/account/${account.id + 1}`)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({
      result: false,
      message: 'Account not found',
    });
  });
  it('should return response with status 400 given successful delete single account with no matched restricted_delete_account permission', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('account_deleter', [
      'restricted_delete_account',
    ]);
    await attachRolesToAccount(account.id, [roleId]);
    const targetAccount = await generateAccount();
    await setRolePermissionRestriction(
      account.id,
      roleId,
      'restricted_delete_account',
      { ids: [targetAccount.id] }
    );
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    await login({
      email: targetAccount.email,
      password: targetAccount.password,
    });
    expect(checkCacheAccountExist(targetAccount.id)).toEqual(true);
    const res = await request(app)
      .delete(`/account/account/${targetAccount.id + 1}`)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({
      result: false,
      message: 'Account not found',
    });
    const newAccountQuery = `SELECT id FROM account WHERE id = $1`;
    const newAccountQueryResult = await pool.query<{ id: number }>(
      newAccountQuery,
      [targetAccount.id]
    );
    expect(newAccountQueryResult.rows.length).toEqual(1);
    expect(checkCacheAccountExist(account.id)).toBe(true);
  });
  it('should get response with status 401 given invalid access token', async () => {
    const accessToken = 'invalid';
    const res = await request(app)
      .delete('/account/account/1')
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${accessToken}`);
    expect(res.statusCode).toEqual(401);
    expect(res.body).toEqual({
      result: false,
      message: 'Invalid access token',
    });
  });
  it('should get response with status 401 given request without access token', async () => {
    const res = await request(app)
      .delete('/account/account/1')
      .set('Accept', 'application/json');
    expect(res.statusCode).toEqual(401);
    expect(res.body).toEqual({
      result: false,
      message: 'Missing access token',
    });
  });
  it('should get response with status 403 given invalid permissions', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('account_creator', ['create_account']);
    await attachRolesToAccount(account.id, [roleId]);
    const targetAccount = await generateAccount();
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    await login({
      email: targetAccount.email,
      password: targetAccount.password,
    });
    expect(checkCacheAccountExist(targetAccount.id)).toEqual(true);
    const res = await request(app)
      .delete(`/account/account/${targetAccount.id}`)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(403);
    expect(res.body).toEqual({ result: false, message: 'Invalid permissions' });
    const newAccountQuery = `SELECT id FROM account WHERE id = $1`;
    const newAccountQueryResult = await pool.query<{ id: number }>(
      newAccountQuery,
      [targetAccount.id]
    );
    expect(newAccountQueryResult.rows.length).toEqual(1);
    expect(checkCacheAccountExist(targetAccount.id)).toEqual(true);
  });
});
