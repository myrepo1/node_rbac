import request from 'supertest';
<<<<<<< Updated upstream
import getApp from '../../../src/app';
import { generateDummyAccessToken } from '../../../src/helper/security';
import {
  clearCacheRole,
  getCachedRolePermissions,
} from '../../../src/service/permission';
import {
  attachRolesToAccount,
  cacheAccountWithoutDb,
  cacheRoleWithoutDb,
  cleanUpAllTest,
  clearAccount,
  clearRole,
  generateAccount,
  generateRole,
} from '../../../src/helper/utility_test';
=======
import getApp from "../../../src/app"
import { clearCacheRole, getCachedRolePermissions } from '../../../src/service/permission';
import { attachRolesToAccount, cacheRoleWithoutDb, cleanUpAllTest, clearAccount, clearRole, generateAccount, generateRole } from '../../../src/helper/utility_test';
>>>>>>> Stashed changes
import { resetAccountData } from '../../../src/service/cache';
import { login } from '../../../src/service/account';
import { SuccessProcessResponse } from '../../../src/type';
import pool from '../../../src/config/pool';

beforeAll(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
});

afterEach(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
  resetAccountData();
});

afterAll(async () => {
  await cleanUpAllTest();
});

describe('Test Update Role', () => {
  const app = getApp();
  it('should return response with status 200 given successful update role', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('role_updater', ['update_role']);
    const targetRoleId = await generateRole('sample_role');
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = { name: 'updated' };
    const res = await request(app)
      .put(`/role/role/${targetRoleId}`)
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: {
        id: targetRoleId,
        name: 'updated',
        description: expect.any(String),
      },
    });
    const updatedRole = await pool.query<{ name: string }>(
      'SELECT name FROM role WHERE id = $1',
      [targetRoleId]
    );
    expect(updatedRole.rows[0].name).toEqual(payload.name);
  });
  it('should return response with status 200 given successful update role with attach and detach permissions', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('role_updater', [
      'update_role',
      'attach_permission_to_role',
      'detach_permission_from_role',
    ]);
    await attachRolesToAccount(account.id, [roleId]);
    const targetRoleId = await generateRole('sample_role', [
      'create_role',
      'update_role',
    ]);
    cacheRoleWithoutDb(targetRoleId, ['create_role', 'update_role']);
    expect(getCachedRolePermissions(targetRoleId)).toEqual(
      expect.arrayContaining(['create_role', 'update_role'])
    );
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = {
      attachPermissions: ['delete_role'],
      detachPermissions: ['update_role'],
    };
    const res = await request(app)
      .put(`/role/role/${targetRoleId}`)
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: null,
    });
    const updatedRoleQuery = `
            SELECT id,(
                SELECT jsonb_agg(permission_name)
                FROM role_permission
                WHERE role_id = role.id
            ) AS permission_names
            FROM role
            WHERE id = $1
        `;
    const updatedRole = await pool.query<{
      id: number;
      name: string;
      permission_name: string[];
    }>(updatedRoleQuery, [targetRoleId]);
    expect(updatedRole.rows.length).toEqual(1);
    expect(updatedRole.rows[0]).toEqual({
      id: targetRoleId,
      permission_names: ['create_role', 'delete_role'],
    });
    expect(getCachedRolePermissions(targetRoleId)).toEqual(
      expect.arrayContaining(['create_role', 'delete_role'])
    );
  });
  it('should return response with status 200 given successful update role with attach only permission', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('role_updater', [
      'update_role',
      'attach_permission_to_role',
    ]);
    await attachRolesToAccount(account.id, [roleId]);
    const targetRoleId = await generateRole('sample_role', [
      'create_role',
      'update_role',
    ]);
    cacheRoleWithoutDb(targetRoleId, ['create_role', 'update_role']);
    expect(getCachedRolePermissions(targetRoleId)).toEqual(
      expect.arrayContaining(['create_role', 'update_role'])
    );
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = {
      attachPermissions: ['delete_role'],
    };
    const res = await request(app)
      .put(`/role/role/${targetRoleId}`)
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: null,
    });
    const updatedRoleQuery = `
            SELECT id,(
                SELECT jsonb_agg(permission_name)
                FROM role_permission
                WHERE role_id = role.id
            ) AS permission_names
            FROM role
            WHERE id = $1
        `;
    const updatedRole = await pool.query<{
      id: number;
      name: string;
      permission_name: string[];
    }>(updatedRoleQuery, [targetRoleId]);
    expect(updatedRole.rows.length).toEqual(1);
    expect(updatedRole.rows[0]).toEqual({
      id: targetRoleId,
      permission_names: ['create_role', 'update_role', 'delete_role'],
    });
    expect(getCachedRolePermissions(targetRoleId)).toEqual(
      expect.arrayContaining(['create_role', 'update_role', 'delete_role'])
    );
  });
  it('should return response with status 200 given successful update role with detach only permission', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('role_updater', [
      'update_role',
      'detach_permission_from_role',
    ]);
    await attachRolesToAccount(account.id, [roleId]);
    const targetRoleId = await generateRole('sample_role', [
      'create_role',
      'update_role',
    ]);
    cacheRoleWithoutDb(targetRoleId, ['create_role', 'update_role']);
    expect(getCachedRolePermissions(targetRoleId)).toEqual(
      expect.arrayContaining(['create_role', 'update_role'])
    );
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = {
      detachPermissions: ['update_role'],
    };
    const res = await request(app)
      .put(`/role/role/${targetRoleId}`)
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: null,
    });
    const updatedRoleQuery = `
            SELECT id,(
                SELECT jsonb_agg(permission_name)
                FROM role_permission
                WHERE role_id = role.id
            ) AS permission_names
            FROM role
            WHERE id = $1
        `;
    const updatedRole = await pool.query<{
      id: number;
      name: string;
      permission_name: string[];
    }>(updatedRoleQuery, [targetRoleId]);
    expect(updatedRole.rows.length).toEqual(1);
    expect(updatedRole.rows[0]).toEqual({
      id: targetRoleId,
      permission_names: ['create_role'],
    });
    expect(getCachedRolePermissions(targetRoleId)).toEqual(
      expect.arrayContaining(['create_role'])
    );
  });
  it('should get response with status 401 given invalid access token', async () => {
    const accessToken = 'invalid';
    const payload = { name: 'dummy', description: 'dummy' };
    const res = await request(app)
      .put('/role/role/3')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${accessToken}`);
    expect(res.statusCode).toEqual(401);
    expect(res.body).toEqual({
      result: false,
      message: 'Invalid access token',
    });
  });
  it('should get response with status 401 given request without access token', async () => {
    const payload = { name: 'dummy', description: 'dummy' };
    const res = await request(app)
      .put('/role/role/3')
      .send(payload)
      .set('Accept', 'application/json');
    expect(res.statusCode).toEqual(401);
    expect(res.body).toEqual({
      result: false,
      message: 'Missing access token',
    });
  });
  it('should get response with status 403 given invalid permissions', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('role_updater', ['create_role']);
    const targetRoleId = await generateRole('sample_role');
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = { name: 'updated' };
    const res = await request(app)
      .put(`/role/role/${targetRoleId}`)
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(403);
    expect(res.body).toEqual({ result: false, message: 'Invalid permissions' });
    const updatedRole = await pool.query<{ name: string }>(
      'SELECT name FROM role WHERE id = $1',
      [targetRoleId]
    );
    expect(updatedRole.rows[0].name).toEqual('sample_role');
  });
  it('should get response with status 400 given invalid payload', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('role_updater', ['update_role']);
    const targetRoleId = await generateRole('sample_role');
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = { name: '' };
    const res = await request(app)
      .put(`/role/role/${targetRoleId}`)
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({
      result: false,
      message: 'Invalid payload',
    });
    const updatedRole = await pool.query<{ name: string }>(
      'SELECT name FROM role WHERE id = $1',
      [targetRoleId]
    );
    expect(updatedRole.rows[0].name).toEqual('sample_role');
  });
});
