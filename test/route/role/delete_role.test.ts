import request from 'supertest';
import getApp from '../../../src/app';
import { clearCacheRole } from '../../../src/service/permission';
import {
  attachRolesToAccount,
  cleanUpAllTest,
  clearAccount,
  clearRole,
  generateAccount,
  generateRole,
} from '../../../src/helper/utility_test';
import { resetAccountData } from '../../../src/service/cache';
import { login } from '../../../src/service/account';
import { SuccessProcessResponse } from '../../../src/type';
import pool from '../../../src/config/pool';

beforeAll(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
});

afterEach(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
  resetAccountData();
});

afterAll(async () => {
  await cleanUpAllTest();
});

describe('Test Delete Role', () => {
  const app = getApp();
  it('should return response with status 200 given successful delete role', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('role_deleter', ['delete_role']);
    const targetRoleId = await generateRole('sample_role');
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const res = await request(app)
      .delete(`/role/role/${targetRoleId}`)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      result: true,
      data: null,
    });
    const deletedRole = await pool.query<{ name: string }>(
      'SELECT name FROM role WHERE id = $1',
      [targetRoleId]
    );
    expect(deletedRole.rows.length).toEqual(0);
  });
  it('should get response with status 401 given invalid access token', async () => {
    const accessToken = 'invalid';
    const res = await request(app)
      .delete('/role/role/3')
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${accessToken}`);
    expect(res.statusCode).toEqual(401);
    expect(res.body).toEqual({
      result: false,
      message: 'Invalid access token',
    });
  });
  it('should get response with status 401 given request without access token', async () => {
    const res = await request(app)
      .delete('/role/role/3')
      .set('Accept', 'application/json');
    expect(res.statusCode).toEqual(401);
    expect(res.body).toEqual({
      result: false,
      message: 'Missing access token',
    });
  });
  it('should get response with status 403 given invalid permissions', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('not_role_deleter', ['list_role']);
    const targetRoleId = await generateRole('sample_role');
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const res = await request(app)
      .delete(`/role/role/${targetRoleId}`)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(403);
    expect(res.body).toEqual({ result: false, message: 'Invalid permissions' });
  });
});
