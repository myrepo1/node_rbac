import request from 'supertest';
import getApp from '../../../src/app';
import { clearCacheRole } from '../../../src/service/permission';
import {
  attachRolesToAccount,
  cleanUpAllTest,
  clearAccount,
  clearRole,
  generateAccount,
  generateRole,
} from '../../../src/helper/utility_test';
import { resetAccountData } from '../../../src/service/cache';
import { login } from '../../../src/service/account';
import { SuccessProcessResponse } from '../../../src/type';
import pool from '../../../src/config/pool';

beforeAll(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
});

afterEach(async () => {
  await clearAccount();
  await clearRole();
  clearCacheRole();
  resetAccountData();
});

afterAll(async () => {
  await cleanUpAllTest();
});

describe('Test Create Role', () => {
  const app = getApp();
  it('should return response with status 201 given successful create role without permissions', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('role_creator', ['create_role']);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = { name: 'dummy', description: 'dummy' };
    const res = await request(app)
      .post('/role/role')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(201);
    expect(res.body).toEqual({
      result: true,
      data: {
        id: expect.any(Number),
        name: payload.name,
        description: payload.description,
      },
    });
    const newRole = await pool.query<{ name: string }>(
      'SELECT name FROM role WHERE id = $1',
      [res.body.data.id]
    );
    expect(newRole.rows.length).toEqual(1);
    expect(newRole.rows[0].name).toEqual(payload.name);
  });
  it('should return response with status 201 given successful create role with its permissions', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('role_creator', [
      'create_role',
      'attach_permission_to_role',
    ]);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = {
      name: 'more_admin',
      description: 'more admin role',
      permissions: ['create_role', 'update_role'],
    };
    const res = await request(app)
      .post('/role/role')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(201);
    expect(res.body).toEqual({
      result: true,
      data: {
        id: expect.any(Number),
        name: payload.name,
        description: payload.description,
      },
    });
    const newRole = await pool.query<{ name: string; permissions: string[] }>(
      `
                SELECT name,
                    coalesce((
                        SELECT json_agg(role_permission.permission_name)
                        FROM role_permission
                        WHERE role_permission.role_id = role.id
                    ),'[]') AS permissions
                FROM role WHERE id = $1`,
      [res.body.data.id]
    );
    expect(newRole.rows.length).toEqual(1);
    expect(newRole.rows[0]).toEqual({
      name: payload.name,
      permissions: expect.arrayContaining(payload.permissions),
    });
  });
  it('should get response with status 401 given invalid access token', async () => {
    const accessToken = 'invalid';
    const payload = { name: 'dummy', description: 'dummy' };
    const res = await request(app)
      .post('/role/role')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${accessToken}`);
    expect(res.statusCode).toEqual(401);
    expect(res.body).toEqual({
      result: false,
      message: 'Invalid access token',
    });
  });
  it('should get response with status 401 given request without access token', async () => {
    const payload = { name: 'dummy', description: 'dummy' };
    const res = await request(app)
      .post('/role/role')
      .send(payload)
      .set('Accept', 'application/json');
    expect(res.statusCode).toEqual(401);
    expect(res.body).toEqual({
      result: false,
      message: 'Missing access token',
    });
  });
  it('should get response with status 403 given invalid permissions', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('roles_viewer', ['list_role']);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = { name: 'dummy', description: 'dummy' };
    const res = await request(app)
      .post('/role/role')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(403);
    expect(res.body).toEqual({ result: false, message: 'Invalid permissions' });
  });
  it('should get response with status 400 given invalid payload', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('role_creator', ['create_role']);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = {};
    const res = await request(app)
      .post('/role/role')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({
      result: false,
      message: 'Invalid payload',
    });
  });
  it('should get response with status 400 given not exist permission', async () => {
    const account = await generateAccount();
    const roleId = await generateRole('role_creator', [
      'create_role',
      'attach_permission_to_role',
    ]);
    await attachRolesToAccount(account.id, [roleId]);
    const loginResult = (await login({
      email: account.email,
      password: account.password,
    })) as SuccessProcessResponse<{
      accessToken: string;
      refreshToken: string;
    }>;
    const payload = {
      name: 'more_admin',
      description: 'more admin role',
      permissions: ['not_exist_permission'],
    };
    const res = await request(app)
      .post('/role/role')
      .send(payload)
      .set('Accept', 'application/json')
      .set('authorization', `Bearer ${loginResult.data.accessToken}`);
    expect(res.statusCode).toEqual(400);
    expect(res.body).toEqual({
      result: false,
      message: 'Failed to create role',
    });
  });
});
