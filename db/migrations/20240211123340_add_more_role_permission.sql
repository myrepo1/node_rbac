-- migrate:up
INSERT INTO permission (name,description) VALUES
('list_role','List Role'),
('get_role_detail','Get Role Detail');

WITH role_data AS (
    SELECT id FROM role WHERE name = 'admin'
)
INSERT INTO role_permission (permission_name,role_id) VALUES
('list_role',(SELECT id FROM role_data)),
('get_role_detail',(SELECT id FROM role_data));
-- migrate:down

