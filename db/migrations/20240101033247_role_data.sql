-- migrate:up
WITH role_data AS (
    INSERT INTO role (name,description) VALUES
('admin','Administrator')
    RETURNING id
)
INSERT INTO role_permission (permission_name,role_id) VALUES
('create_role',(SELECT id FROM role_data)),
('update_role',(SELECT id FROM role_data)),
('attach_permission_to_role',(SELECT id FROM role_data)),
('detach_permission_from_role',(SELECT id FROM role_data)),
('delete_role',(SELECT id FROM role_data)),
('create_account',(SELECT id FROM role_data)),
('update_account',(SELECT id FROM role_data)),
('delete_account',(SELECT id FROM role_data)),
('list_account',(SELECT id FROM role_data)),
('get_account_detail',(SELECT id FROM role_data))
-- migrate:down

