-- migrate:up
INSERT INTO permission (name,description) VALUES
('attach_role_to_account','Attach roles to certain account'),
('detach_role_from_account','Detach roles from certain account');

WITH role_data AS (
    SELECT id FROM role WHERE name = 'admin'
)
INSERT INTO role_permission (permission_name,role_id) VALUES
('attach_role_to_account', (SELECT id FROM role_data)),
('detach_role_from_account', (SELECT id FROM role_data));
-- migrate:down

