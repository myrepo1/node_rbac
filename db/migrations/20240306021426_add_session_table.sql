-- migrate:up
-- Create table session with primary key is uuid

CREATE TABLE session (
    id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
    account_id INTEGER REFERENCES account(id) ON DELETE CASCADE,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    active BOOLEAN NOT NULL,
    role_config JSONB NOT NULL DEFAULT '{}'::jsonb -- config payload {roleIds:int[],permissionRestrictions:unknown}}
);

-- migrate:down

