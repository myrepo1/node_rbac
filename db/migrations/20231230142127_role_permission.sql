-- migrate:up
CREATE TABLE role_permission (
    id SERIAL PRIMARY KEY,
    role_id INT NOT NULL,
    permission_name TEXT NOT NULL,
    FOREIGN KEY (role_id) REFERENCES role(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (permission_name) REFERENCES permission(name) ON UPDATE CASCADE ON DELETE CASCADE,
    UNIQUE (role_id, permission_name)
);

-- migrate:down
