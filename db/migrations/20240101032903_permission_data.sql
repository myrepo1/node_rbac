-- migrate:up
INSERT INTO permission (name,description) VALUES
('create_role','Create a new role'),
('update_role','Update an existing role except attached permission'),
('attach_permission_to_role','Attach permission to an existing role'),
('detach_permission_from_role','Detach permission from an existing role'),
('delete_role','Delete an existing role'),
('create_account','Create a new account'),
('list_account','List all accounts'),
('get_account_detail','Get detail of an existing account'),
('restricted_get_account_detail','Get detail of certain account based on accountId'),
('update_account','Update an existing account'),
('restricted_update_account','Update certain account based on accountId'),
('delete_account','Delete an existing account'),
('restricted_delete_account','Delete certain account based on accountId'),
('list_permissions','List all permissions')
-- migrate:down
