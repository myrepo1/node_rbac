-- migrate:up
-- Create table role with foreign key to table permission
CREATE TABLE role (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL UNIQUE,
    description TEXT
);

-- migrate:down

