-- migrate:up
CREATE TABLE role_restriction (
    account_role_id INTEGER REFERENCES account_role(id) ON DELETE CASCADE,
    role_permission_id INTEGER REFERENCES role_permission(id) ON DELETE CASCADE,
    permission_restriction JSONB NOT NULL DEFAULT '{}'::jsonb,
    PRIMARY KEY (account_role_id, role_permission_id)
)

-- migrate:down

