import 'dotenv/config';

const env = {
  pgHost: process.env.POSTGRES_HOST as string,
  pgUser: process.env.POSTGRES_USER as string,
  pgPass: process.env.POSTGRES_PASSWORD as string,
  pgPort: Number(process.env.POSTGRES_PORT) || 5432,
  pgDb: process.env.POSTGRES_DB as string,
  jwtSecret: (process.env.JWT_SECRET as string) || 'secret',
  port: Number(process.env.PORT as string) || 4000,
  appHost: process.env.APP_HOST || 'http://localhost:4000',
};

export default env;
