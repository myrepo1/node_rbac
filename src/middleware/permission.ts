import { Response, NextFunction } from 'express';
import {
  restrictedPermissionValidators,
  validateAccess,
} from '../service/permission';
import { AccessName, CustomRequest, IContext } from '../type';

/**
 * Generate permission validator middleware
 * @param permission Name of access. One access could contain multiple permissions
 * @param ctxModifierFn ctx modifier function, used to modify context based on permission validator result
 * @returns
 */
export function getPermissionMiddleWare(
  permission: AccessName,
  /**
   * @deprecated
   */
  ctxModifierFn?: (ctx: IContext) => void
) {
  return (req: CustomRequest, res: Response, next: NextFunction) => {
    if (!req.context)
      return res
        .status(500)
        .json({ status: false, message: 'Missing context' });
    if (!validateAccess(permission, req.context))
      return res
        .status(403)
        .json({ result: false, message: 'Invalid permissions' });

    /**
     * Currently only one permission restriction may be set
     */
    if (req.context.matchedPermissions.length > 0) {
      req.context.matchedPermissions.some((permission) => {
        if ((restrictedPermissionValidators as any)[permission]) {
          (req.context as IContext).hasRestriction = true;
          (req.context as IContext).restriction =
            (req.context as IContext).sessionRestrictions[permission] || {};
          return true;
        }
        return false;
      });
    }
    next();
  };
}
