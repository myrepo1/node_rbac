import { Response, NextFunction } from 'express';
import { validateJWT } from '../helper/security';
import { CustomJWTPayload, CustomRequest } from '../type';
import { getSingleAccountCache, setSingleAccountData } from '../service/cache';
import { cacheRole } from '../service/permission';
import { getSession } from '../service/session';

/**
 * Verify JWT token. If valid, attach session data to context.
 * @param req
 * @param res
 * @param next
 * @returns
 */
export async function authMiddleware(
  req: CustomRequest,
  res: Response,
  next: NextFunction
) {
  if (!req.context)
    return res.status(500).json({ status: false, message: 'Missing context' });
  const accessToken = req.headers['authorization'];

  if (!accessToken) {
    return res
      .status(401)
      .json({ result: false, message: 'Missing access token' });
  }

  try {
    const decodedToken = validateJWT(
      accessToken.replace('Bearer ', '')
    ) as CustomJWTPayload;
    if (decodedToken.type !== 'accessToken') {
      return res
        .status(401)
        .json({ result: false, message: 'Invalid access token' });
    }
    req.context.accountId = decodedToken.id;
    req.context.sessionId = decodedToken.sessionId;
    const cacheAccount = getSingleAccountCache(decodedToken.id);
    if (!cacheAccount) {
      const session = await getSession(decodedToken.id, decodedToken.sessionId);
      if (!session)
        return res
          .status(401)
          .json({ result: false, message: 'Invalid access token' });
      await cacheRole(session.role_config.roleIds);
      setSingleAccountData(decodedToken.id, {roleIds: session.role_config.roleIds});
      req.context.roleIds = session.role_config.roleIds;
      req.context.sessionRestrictions =
        session.role_config.permissionRestrictions;
    } else {
      req.context.roleIds = cacheAccount.roleIds;
      req.context.sessionRestrictions = cacheAccount.permissionRestrictions;
    }

    next();
  } catch (error) {
    return res
      .status(401)
      .json({ result: false, message: 'Invalid access token' });
  }
}
