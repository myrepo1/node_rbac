import { Request, Response, NextFunction } from 'express';
import { CustomRequest } from '../type';

/**
 * Attach context to request.
 * Context are used for passing data between middlewares.
 * @param req
 * @param res
 * @param next
 */
export function attachContextMiddleware(
  req: Request & CustomRequest,
  res: Response,
  next: NextFunction
) {
  req.context = {
    accountId: 0,
    sessionId: '',
    roleIds: [],
    matchedPermissions: [],
    sessionRestrictions: {},
    hasRestriction: false,
    restriction: null,
  };
  next();
}
