import { Pool, PoolClient } from 'pg';
import * as z from 'zod';

import pool from '../config/pool';
import {
  IContext,
  IRole,
  IRoleDetail,
  PermissionName,
  ProcessResponse,
} from '../type';
import { getIncrFunc } from '../helper/general';
import {
  detachCacheRole,
  unsetRole,
  updateAttachedCacheRole,
} from './permission';

const roleSchema = z.object({
  name: z.string().min(1),
  description: z.string().nullish(),
  permissions: z.array(z.string().min(1)).nullish(),
});

const updateRoleSchema = z.object({
  id: z.number().min(1),
  name: z.string().min(1).optional(),
  description: z.string().nullish(),
  attachPermissions: z.array(z.string().min(1)).nullish(),
  detachPermissions: z.array(z.string().min(1)).nullish(),
});

export async function listRoles(): Promise<ProcessResponse<IRole[]>> {
  try {
    const roleListQuery = `
                SELECT id,name
                FROM role
        `;
    const roleListQueryResult = await pool.query<IRole>(roleListQuery);
    return {
      result: true,
      data: roleListQueryResult.rows,
    };
  } catch (error) {
    return {
      result: false,
      message: 'Failed to get role list',
    };
  }
}

export async function getDetailRole(
  roleId: number
): Promise<ProcessResponse<IRoleDetail>> {
  try {
    const roleDetailQuery = `
                SELECT id,name,description,
                    coalesce((
                        SELECT json_agg(role_permission.permission_name)
                        FROM role_permission
                        WHERE role_permission.role_id = role.id
                      ),'[]') AS permissions
                FROM role
                WHERE role.id = $1
        `;
    const roleDetailQueryResult = await pool.query<IRoleDetail>(
      roleDetailQuery,
      [roleId]
    );
    if (!roleDetailQueryResult.rows.length)
      return {
        result: false,
        message: 'Role not found',
      };
    return {
      result: true,
      data: roleDetailQueryResult.rows[0],
    };
  } catch (error) {
    return {
      result: false,
      message: 'Failed to get role detail',
    };
  }
}

export async function createRole(
  payload: z.infer<typeof roleSchema>,
  ctx: IContext
): Promise<ProcessResponse<Omit<IRoleDetail, 'permissions'>>> {
  const parseResult = roleSchema.safeParse(payload);
  if (!parseResult.success)
    return { result: false, message: 'Invalid payload' };
  const queryValueParams: (string | null | undefined)[] = [];
  try {
    if (
      ctx.matchedPermissions.includes('attach_permission_to_role') &&
      parseResult.data.permissions?.length
    ) {
      const incrFunc = getIncrFunc(2);
      queryValueParams.push(
        parseResult.data.name,
        parseResult.data.description
      );
      const permissionValueClause: string[] = [];
      parseResult.data.permissions.forEach((permission) => {
        permissionValueClause.push(
          `((SELECT id FROM new_role),$${incrFunc()})`
        );
        queryValueParams.push(permission);
      });
      const newRoleQuery = `
            WITH new_role AS (
                INSERT INTO role (name,description)
                VALUES ($1,$2)
                RETURNING id
            )
            INSERT INTO role_permission (role_id,permission_name)
            VALUES ${permissionValueClause.join(',')}
            RETURNING role_id
        `;
      const newRoleQueryResult = await pool.query<{ role_id: number }>(
        newRoleQuery,
        queryValueParams
      );
      return {
        result: true,
        data: {
          id: newRoleQueryResult.rows[0].role_id,
          name: parseResult.data.name,
          description: parseResult.data.description,
        },
      };
    } else {
      const newRoleQuery = `
            INSERT INTO role (name,description)
            VALUES ($1,$2)
            RETURNING id
        `;
      const newRoleQueryResult = await pool.query<{ id: number }>(
        newRoleQuery,
        [parseResult.data.name, parseResult.data.description]
      );
      return {
        result: true,
        data: {
          id: newRoleQueryResult.rows[0].id,
          name: parseResult.data.name,
          description: parseResult.data.description,
        },
      };
    }
  } catch (error) {
    return {
      result: false,
      message: 'Failed to create role',
    };
  }
}

export async function updateRole(
  payload: z.infer<typeof updateRoleSchema>,
  ctx: IContext
): Promise<ProcessResponse<Omit<IRoleDetail, 'permissions'> | null>> {
  const parseResult = updateRoleSchema.safeParse(payload);
  if (!parseResult.success)
    return { result: false, message: 'Invalid payload' };
  const updateValueParams: (string | number | null)[] = [];
  const updateValueClause: string[] = [];
  const {
    id,
    attachPermissions = [],
    detachPermissions = [],
    ...updateData
  } = parseResult.data;
  let client: Pool | PoolClient = pool;
  let useTransaction = -1;
  let updateResult: Omit<IRoleDetail, 'permissions'> | null = null;
  let failedMessageResult: string[] = [];
  const incrFunc = getIncrFunc();
  Object.entries(updateData).forEach(([key, value]) => {
    if (value !== undefined) {
      updateValueClause.push(`${key} = $${incrFunc()}`);
      updateValueParams.push(value);
    }
  });
  const attachPermissionAction = Boolean(
    attachPermissions?.length &&
      ctx.matchedPermissions.includes('attach_permission_to_role')
  );
  const detachPermissionAction = Boolean(
    detachPermissions?.length &&
      ctx.matchedPermissions.includes('detach_permission_from_role')
  );
  useTransaction +=
    Number(Boolean(updateValueClause.length)) +
    Number(attachPermissionAction) +
    Number(detachPermissionAction);
  if (useTransaction > 0) {
    client = await pool.connect();
    await client.query('BEGIN');
  } else if (useTransaction < 0) {
    return { result: false, message: 'Nothing to update' };
  }
  try {
    if (updateValueClause.length) {
      const updateRoleQuery = `
          UPDATE role
          SET ${updateValueClause.join(', ')}
          WHERE id = $${incrFunc()}
          RETURNING id,name,description
        `;
      const updateRoleQueryResult = await client.query<
        Omit<IRoleDetail, 'permissions'>
      >(updateRoleQuery, [...updateValueParams, id]);
      if (updateRoleQueryResult.rowCount) {
        updateResult = {
          id: updateRoleQueryResult.rows[0].id,
          name: updateRoleQueryResult.rows[0].name,
          description: updateRoleQueryResult.rows[0].description,
        };
      } else {
        failedMessageResult.push('Role not found');
      }
    }
    if (attachPermissionAction) {
      const attachResult = await attachPermission(
        { roleId: id, permissions: attachPermissions! },
        client
      );
      if (!attachResult.result) {
        failedMessageResult.push(attachResult.message);
      }
    }
    if (detachPermissionAction) {
      const detachResult = await detachPermission(
        { roleId: id, permissions: detachPermissions! },
        client
      );
      if (!detachResult.result) {
        failedMessageResult.push(detachResult.message);
      }
    }
  } catch (error) {
    console.error(error);
    failedMessageResult.push((error as Error).message);
    if (useTransaction) {
      await client.query('ROLLBACK');
    }
    return {
      result: false,
      message: failedMessageResult.join(',') || 'Failed to update role',
    };
  }
  if (failedMessageResult.length) {
    if (useTransaction) {
      await client.query('ROLLBACK');
    }
    return {
      result: false,
      message: failedMessageResult.join(',') || 'Failed to update role',
    };
  }
  if (useTransaction) {
    await client.query('COMMIT');
    (client as PoolClient).release();
  }
  return { result: true, data: updateResult };
}

export async function deleteRole(id: number): Promise<ProcessResponse> {
  const deleteRoleQuery = `
        DELETE FROM role
        WHERE id = $1
        RETURNING id
    `;
  const deleteRoleQueryResult = await pool.query<{ id: number }>(
    deleteRoleQuery,
    [id]
  );
  if (!deleteRoleQueryResult.rowCount)
    return { result: false, message: 'Role not found' };
  unsetRole(deleteRoleQueryResult.rows[0].id);

  return { result: true, data: null };
}

async function attachPermission(
  payload: { roleId: number; permissions: string[] },
  client: Pool | PoolClient
): Promise<ProcessResponse> {
  try {
    const attachPermissionValueClause: string[] = [];
    const attachPermissionParams: (string | number)[] = [payload.roleId];
    const incrFunc = getIncrFunc(1);
    payload.permissions.forEach((permission: string) => {
      attachPermissionValueClause.push(`($1,$${incrFunc()})`);
      attachPermissionParams.push(permission);
    });
    const attachPermissionQuery = `INSERT INTO role_permission (role_id,permission_name) VALUES ${attachPermissionValueClause.join(
      ','
    )}`;
    await client.query(attachPermissionQuery, attachPermissionParams);
    updateAttachedCacheRole(
      payload.roleId,
      payload.permissions as PermissionName[]
    );
    return { result: true, data: null };
  } catch (error) {
    console.log(error);
    return { result: false, message: 'Failed to attach permission' };
  }
}

async function detachPermission(
  payload: { roleId: number; permissions: string[] },
  client: Pool | PoolClient
): Promise<ProcessResponse> {
  try {
    const detachRoleParams: (string[] | number)[] = [payload.roleId, []];
    payload.permissions.forEach((permission: string) => {
      (detachRoleParams[1] as string[]).push(permission);
    });
    const detachPermissionQuery = `DELETE FROM role_permission WHERE role_id = $1 AND permission_name = ANY($2)`;
    const detachPermissionResult = await client.query(
      detachPermissionQuery,
      detachRoleParams
    );
    if (!detachPermissionResult.rowCount)
      return { result: false, message: 'Failed to detach permission' };
    detachCacheRole(payload.roleId, payload.permissions);
    return { result: true, data: null };
  } catch (error) {
    return { result: false, message: 'Failed to detach permission' };
  }
}
