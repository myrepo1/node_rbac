import { AccountCacheData, CacheRoleData } from '../type';

const cacheData: {
  roleData: CacheRoleData;
  account: Record<number, AccountCacheData>;
} = {
  account: {},
  roleData: {},
};

export function getRoleCache() {
  return cacheData.roleData;
}

export function setRoleData(data: CacheRoleData) {
  cacheData.roleData = data;
}

export function getSingleAccountCache(accountId:number) {
  return cacheData.account[accountId];
}

/**
 * Set cache for account data.
 * Useful for testing purpose
 * @param data
 */
export function resetAccountData() {
  cacheData.account = {};
}

/**
 * Set cache for single account data.
 * Currently only used for cache roleIds
 * @param accountId
 * @param roleIds
 */
export function setSingleAccountData(accountId: number, data:{roleIds: number[],permissionRestrictions?:Record<PermissionName,unknown>}) {
  const currentCache = cacheData.account[accountId];
  const { roleIds=currentCache?.roleIds||[],permissionRestrictions=currentCache?.permissionRestrictions||{} } = data;  
  cacheData.account[accountId] = {
    roleIds,
    permissionRestrictions
  };
}

/**
 * Remove account cache given certain accountId
 * @param accountId
 */
export function removeAccountCache(accountId: number) {
  delete cacheData.account[accountId];
}
