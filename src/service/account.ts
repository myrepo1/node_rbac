import { Pool, PoolClient } from 'pg';
import * as z from 'zod';

import pool from '../config/pool';
import {
  AccountWithRoleConfig,
  CustomJWTPayload,
  IAccount,
  IContext,
  ISingleAccount,
  ProcessResponse,
} from '../type';
import { generateJWT, validateJWT } from '../helper/security';
import { getIncrFunc } from '../helper/general';
import { cacheRole, restrictedPermissionValidators } from './permission';
import {
  getSingleAccountCache,
  removeAccountCache,
  setSingleAccountData,
} from './cache';
import {
  addSession,
  getSession,
  stopSession,
  updateSessionConfig,
} from './session';

const loginSchema = z.object({
  email: z.string().min(1),
  password: z.string().min(1),
});

const createAccountSchema = z.object({
  name: z.string().min(1),
  email: z.string().email(),
  password: z.string().min(1),
  attachRoles: z.array(z.number().min(1)).optional(),
});

const updateAccountSchema = createAccountSchema.partial().extend({
  id: z.number().min(1),
  detachRoles: z.array(z.number().min(1)).optional(),
});

/**
 * Authenticates a user with the provided email and password, and generates
 * access and refresh tokens.
 *
 * @param {Object} payload - An object containing the email and password of the user.
 * @param {string} payload.email - The email of the user.
 * @param {string} payload.password - The password of the user.
 * @return {Promise<ProcessResponse<{ accessToken: string; refreshToken: string }>>} An object containing the result of the operation and the generated tokens.
 */
export async function login(
  payload: z.infer<typeof loginSchema>
): Promise<ProcessResponse<{ accessToken: string; refreshToken: string }>> {
  // Parse the provided payload using the loginSchema.
  const parseResult = loginSchema.safeParse(payload);
  if (!parseResult.success)
    return { result: false, message: 'Invalid email or password' };
  try {
    // Retrieve the account and its role configuration data for the provided email and password.
    const accountData = await getAccount({
      email: parseResult.data.email,
      password: parseResult.data.password,
    });
    if (!accountData)
      return { result: false, message: 'Invalid email or password' };
    // Cache the role IDs and add a session for the account.
    const [_, { id: sessionId }] = await Promise.all([
      cacheRole(accountData.role_ids),
      addSession({
        accountId: accountData.id,
        config: {
          roleIds: accountData.role_ids,
          permissionRestrictions: accountData.permission_restrictions,
        },
      }),
    ]);
    // Store the role IDs and permission restrictions in the account cache.
    setSingleAccountData(accountData.id,{roleIds:accountData.role_ids,permissionRestrictions: accountData.permission_restrictions});
    // Generate the access token with the account ID, role IDs, and session ID.
    const accessToken = generateJWT(
      {
        id: accountData.id,
        roles: accountData.role_ids,
        sessionId,
        type: 'accessToken',
      },
      '1h'
    );
    // Generate the refresh token with the account ID and session ID.
    const refreshToken = generateJWT(
      { id: accountData.id, type: 'refreshToken', sessionId },
      '7d'
    );
    return {
      result: true,
      data: { accessToken, refreshToken },
    };
  } catch (error) {
    return { result: false, message: 'Invalid email or password' };
  }
}

/**
 * Refreshes the access token and refresh token using the provided refresh token.
 * @param refreshToken - The refresh token to use for refreshing the access and refresh tokens.
 * @returns A promise that resolves to a ProcessResponse object with the new access and refresh tokens,
 * or an error message if the refresh token is invalid.
 */
export async function refresh(
  refreshToken: string
): Promise<ProcessResponse<{ accessToken: string; refreshToken: string }>> {
  // Check if the refresh token is a valid string.
  if (!z.string().min(1).safeParse(refreshToken).success)
    return { result: false, message: 'Invalid refresh token' };
  try {
    const decodedToken = validateJWT(refreshToken) as CustomJWTPayload;
    if (decodedToken.type !== 'refreshToken')
      return { result: false, message: 'Invalid refresh token' };
    // Get the session data using the account ID and session ID from the decoded token.
    const sessionData = await getSession(
      decodedToken.id,
      decodedToken.sessionId
    );
    if (!sessionData)
      return { result: false, message: 'Invalid refresh token' };
    await cacheRole(sessionData.role_config.roleIds);

    setSingleAccountData(decodedToken.id,{roleIds:sessionData.role_config.roleIds,permissionRestrictions: sessionData.role_config.permissionRestrictions})
    // Generate a new access token using the account ID, role IDs, and session ID from the decoded token.
    const token = generateJWT(
      {
        id: decodedToken.id,
        roles: sessionData.role_config.roleIds,
        type: 'accessToken',
        sessionId: decodedToken.sessionId,
      },
      '1h' // The access token is valid for 1 hour.
    );

    // Return a success response with the new access and refresh tokens.
    return {
      result: true,
      data: { accessToken: token, refreshToken },
    };
  } catch (error) {
    // If there's an error, return an error response.
    return { result: false, message: 'Invalid refresh token' };
  }
}

export async function logOut(
  accountId: number,
  sessionId: string
): Promise<ProcessResponse> {
  try {
    if (
      z.number().safeParse(accountId).success &&
      z.string().safeParse(sessionId).success
    ) {
      await stopSession(accountId, sessionId);
      removeAccountCache(accountId);
      return { result: true, data: null };
    }
    return { result: false, message: 'Invalid payload' };
  } catch (error) {
    return { result: false, message: 'Failed remove session' };
  }
}

/**
 * Get single account details including its role and permissions
 * @param id
 * @param ctx
 * @returns
 */
export async function getSingleAccount(
  id: number,
  ctx: IContext
): Promise<ProcessResponse<ISingleAccount>> {
  if (ctx.hasRestriction) {
    const restrictionValidator =
      restrictedPermissionValidators.restricted_get_account_detail;
    const parseResult = restrictionValidator.safeParse(ctx.restriction);
    if (!parseResult.success || !parseResult.data.ids.includes(id)) {
      return { result: false, message: 'Account not found' };
    }
  }
  try {
    const accountQuery = `
    SELECT id,email,name,
    (
        SELECT jsonb_agg(role_obj)
        FROM (
            SELECT jsonb_build_object('role',role.name,'permissions',array_agg(role_permission.permission_name)) AS role_obj
            FROM account_role
            JOIN role ON account_role.role_id = role.id
            JOIN role_permission ON role.id = role_permission.role_id
            WHERE account_role.account_id = account.id
            GROUP BY role.name
        ) role_data
    ) AS roles
    FROM account
    WHERE id = $1`;
    const accountQueryResult = await pool.query<ISingleAccount>(accountQuery, [
      id,
    ]);
    if (!accountQueryResult.rows.length)
      return { result: false, message: 'Account not found' };
    return { result: true, data: accountQueryResult.rows[0] };
  } catch (error) {
    return { result: false, message: 'Failed to get account' };
  }
}

/**
 * List all accounts
 * @returns
 */
export async function getAccounts(): Promise<ProcessResponse<IAccount[]>> {
  try {
    const accountListQuery = `
        SELECT id,email,name
        FROM account
    `;
    const userListQueryResult = await pool.query<IAccount>(accountListQuery);
    return { result: true, data: userListQueryResult.rows };
  } catch (error) {
    return { result: false, message: 'Failed to get account list' };
  }
}

/**
 * Create new account. If attachRoles is provided, it will also attach the roles to the account
 * @param payload
 * @param ctx
 * @returns
 */
export async function createAccount(
  payload: z.infer<typeof createAccountSchema>,
  ctx: IContext
): Promise<ProcessResponse<IAccount>> {
  const parseResult = createAccountSchema.safeParse(payload);
  if (!parseResult.success)
    return { result: false, message: 'Invalid payload' };
  try {
    if (
      ctx.matchedPermissions.includes('attach_role_to_account') &&
      parseResult.data.attachRoles?.length
    ) {
      const queryValueParams: (string | number | null | undefined)[] = [
        parseResult.data.email,
        parseResult.data.name,
        parseResult.data.password,
      ];
      const queryValueClause: string[] = [];
      const incrFunc = getIncrFunc(3);
      parseResult.data.attachRoles.forEach((roleId) => {
        queryValueClause.push(`((SELECT id FROM new_account),$${incrFunc()})`);
        queryValueParams.push(roleId);
      });
      const createAccountQuery = `
      WITH new_account AS (
        INSERT INTO account (email, name, password)
        VALUES ($1, $2, crypt($3, gen_salt('bf')))
        RETURNING id,email,name
      ), new_account_role AS (
        INSERT INTO account_role (account_id,role_id)
      VALUES ${queryValueClause.join(',')}
      )
      SELECT * FROM new_account
      `;
      const createAccountQueryResult = await pool.query<IAccount>(
        createAccountQuery,
        [...queryValueParams]
      );
      return { result: true, data: createAccountQueryResult.rows[0] };
    } else {
      const createAccountQuery = `
          INSERT INTO account (email, name, password)
          VALUES ($1, $2, crypt($3, gen_salt('bf')))
          RETURNING id,email,name`;
      const createAccountQueryResult = await pool.query<IAccount>(
        createAccountQuery,
        [
          parseResult.data.email,
          parseResult.data.name,
          parseResult.data.password,
        ]
      );
      return { result: true, data: createAccountQueryResult.rows[0] };
    }
  } catch (error) {
    return { result: false, message: 'Email already exists or invalid data' };
  }
}

/**
 * Update account. If attachRoles or detachRoles is provided, it will also attach or detach the roles to the account
 * Modification to attached roles will trigger update active session config
 * @param payload
 * @param ctx
 * @returns
 */
export async function updateAccount(
  payload: z.infer<typeof updateAccountSchema>,
  ctx: IContext
): Promise<ProcessResponse<IAccount | null>> {
  const parseResult = updateAccountSchema.safeParse(payload);
  if (!parseResult.success)
    return { result: false, message: 'Invalid payload' };
  const { id, attachRoles = [], detachRoles = [], ...data } = parseResult.data;

  // Validate restriction if exist
  if (ctx.hasRestriction) {
    const restrictionValidator =
      restrictedPermissionValidators.restricted_update_account;
    const parseResult = restrictionValidator.safeParse(ctx.restriction);
    if (!parseResult.success || !parseResult.data.ids.includes(id)) {
      return { result: false, message: 'Account not found' };
    }
  }

  const updateClause: string[] = [];
  const updateValues: (string | number | null)[] = [];

  // Prepare pool for transaction if necessary
  let client: Pool | PoolClient = pool;
  let useTransaction = false;
  let updateResult: IAccount | null = null;
  let failedMessageResult: string[] = [];
  const incrFunc = getIncrFunc();
  for (const [key, value] of Object.entries(data)) {
    if (key === 'password' && value !== undefined) {
      updateClause.push(`${key} = crypt($${incrFunc()}, gen_salt('bf'))`);
      updateValues.push(value);
    } else if (value !== undefined) {
      updateClause.push(`${key} = $${incrFunc()}`);
      updateValues.push(value);
    }
  }
  const attachRoleToAccountAction = Boolean(
    attachRoles?.length &&
      ctx.matchedPermissions.includes('attach_role_to_account')
  );
  const detachRoleFromAccountAction = Boolean(
    detachRoles?.length &&
      ctx.matchedPermissions.includes('detach_role_from_account')
  );
  if (attachRoleToAccountAction || detachRoleFromAccountAction)
    useTransaction = true;
  if (useTransaction) {
    client = await pool.connect();
    await client.query('BEGIN');
  } else if (useTransaction && !updateValues.length) {
    return { result: false, message: 'Nothing to update' };
  }
  try {
    // Update account
    if (updateClause.length) {
      const updateAccountQuery = `
          UPDATE account
          SET ${updateClause.join(', ')}
          WHERE id = $${incrFunc()}
          RETURNING id,email,name
      `;
      const updateAccountQueryResult = await client.query<IAccount>(
        updateAccountQuery,
        [...updateValues, id]
      );
      if (!updateAccountQueryResult.rowCount)
        return { result: false, message: 'Account not found' };
      updateResult = updateAccountQueryResult.rows[0];
    }

    // Attach role to account
    if (attachRoleToAccountAction) {
      const attachResult = await attachRoleToAccount(id, attachRoles, client);
      if (attachResult) {
        failedMessageResult.push(attachResult);
      }
    }

    // Detach role from account
    if (detachRoleFromAccountAction) {
      const detachResult = await detachRoleFromAccount(id, detachRoles, client);
      if (detachResult) {
        failedMessageResult.push(detachResult);
      }
    }
    if (useTransaction) {
      // Update session config
      const updateSessionResult = await updateSessionConfig(id, client);
      if (updateSessionResult) {
        failedMessageResult.push(updateSessionResult);
      }
    }
  } catch (error) {
    failedMessageResult.push((error as Error).message);
    if (useTransaction) {
      await client.query('ROLLBACK');
      (client as PoolClient).release();
    }
    return {
      result: false,
      message: failedMessageResult.join(',') || 'Failed to update role',
    };
  }
  if (failedMessageResult.length) {
    if (useTransaction) {
      await client.query('ROLLBACK');
      (client as PoolClient).release();
    }
    return {
      result: false,
      message: failedMessageResult.join(',') || 'Failed to update role',
    };
  }
  if (useTransaction) {
    await client.query('COMMIT');
    (client as PoolClient).release();
  }
  return { result: true, data: updateResult };
}

/**
 * Delete account, including remove its cache
 * @param id
 * @param ctx
 * @returns
 */
export async function deleteAccount(
  id: number,
  ctx: IContext
): Promise<ProcessResponse> {
  if (ctx.hasRestriction) {
    const restrictionValidator =
      restrictedPermissionValidators.restricted_get_account_detail;
    const parseResult = restrictionValidator.safeParse(ctx.restriction);
    if (!parseResult.success || !parseResult.data.ids.includes(id)) {
      return { result: false, message: 'Account not found' };
    }
  }
  const deleteAccountQuery = `
        DELETE FROM account
        WHERE id = $1
    `;
  const deleteAccountQueryResult = await pool.query(deleteAccountQuery, [id]);
  if (!deleteAccountQueryResult.rowCount)
    return { result: false, message: 'Account not found' };
  removeAccountCache(id);
  return { result: true, data: null };
}

/**
 * Get account with role config
 * @param params { id: number } | { email: string; password: string }
 * @returns Promise<AccountWithRoleConfig> Account data with its role config
 */
export async function getAccount(
  params: { id: number } | { email: string; password: string }
): Promise<AccountWithRoleConfig> {
  const { query, queryParams } = getAccountWithRoleConfigQuery(params);
  const accountQueryResult = await pool.query<AccountWithRoleConfig>(
    query,
    queryParams
  );
  return accountQueryResult.rows[0];
}

export function checkCacheAccountExist(accountId: number) {
  return !!getSingleAccountCache(accountId);
}

/**
 * Attach roles to certain account.
 * Update cache account if exist
 * @param accountId
 * @param roleIds
 * @param client
 * @returns
 */
async function attachRoleToAccount(
  accountId: number,
  roleIds: number[],
  client: Pool | PoolClient
) {
  try {
    const attachRoleValueClause: string[] = [];
    const attachRoleParams: number[] = [accountId];
    const incrFunc = getIncrFunc(1);
    roleIds.forEach((roleId: number) => {
      attachRoleValueClause.push(`($1,$${incrFunc()})`);
      attachRoleParams.push(roleId);
    });
    const attachRoleQuery = `INSERT INTO account_role (account_id,role_id) VALUES ${attachRoleValueClause.join(
      ','
    )}`;
    await client.query(attachRoleQuery, attachRoleParams);
    const cachedAccount = getSingleAccountCache(accountId);
    if (cachedAccount) {
      setSingleAccountData(
        accountId,
        {roleIds:Array.from(new Set([...cachedAccount.roleIds, ...roleIds])),
        permissionRestrictions:cachedAccount.permissionRestrictions
      }
      );
    }
    return null;
  } catch (error) {
    return (error as Error).message;
  }
}

/**
 * Detach roles to certain account.
 * Update cache account if exist
 * @param accountId
 * @param roleIds
 * @param client
 * @returns
 */
async function detachRoleFromAccount(
  accountId: number,
  roleIds: number[],
  client: Pool | PoolClient
) {
  try {
    const detachRoleQuery = `DELETE FROM account_role WHERE account_id = $1 AND role_id = ANY($2)`;
    await client.query(detachRoleQuery, [accountId, roleIds]);
    const cachedAccount = getSingleAccountCache(accountId);
    if (cachedAccount) {
      setSingleAccountData(
        accountId,
        {roleIds:cachedAccount.roleIds.filter((i) => !roleIds.includes(i)),
        permissionRestrictions: cachedAccount.permissionRestrictions}
      );
    }
    return null;
  } catch (error) {
    return (error as Error).message;
  }
}

/**
 * Generate query for get account using accountId or email and password
 * Account data included its roles and permission restrictions
 * @param params { id: number } | { email: string; password: string }
 * @returns { query: string, queryParams: any[] } An object containing the query and the query parameters
 */
export function getAccountWithRoleConfigQuery(
  params: { id: number } | { email: string; password: string }
) {
  const whereClause =
    'id' in params
      ? 'WHERE id = $1'
      : 'WHERE email = $1 AND password = crypt($2, password)';
  const queryParams =
    'id' in params ? [params.id] : [params.email, params.password];
  return {
    query: `
  SELECT 
    id,
    array_remove(array_agg(distinct role_restriction_data.role_id),NULL) as role_ids,
	  jsonb_strip_nulls(jsonb_object_agg(coalesce(role_restriction_data.permission_name,'empty'),role_restriction_data.permission_restriction)) as permission_restrictions
  FROM account a
  LEFT JOIN LATERAL (
    SELECT
      ar.role_id,
      rp.permission_name,
      rr.permission_restriction,
      case when rr.permission_restriction is null then null else json_build_object('permission',rp.permission_name,'restriction',rr.permission_restriction) end role_restriction_data
    FROM account_role ar
    LEFT JOIN role_restriction rr
    ON ar.id = rr.account_role_id
    LEFT JOIN role_permission rp
    ON rr.role_permission_id = rp.id
    WHERE ar.account_id = a.id    
  ) role_restriction_data
  ON TRUE 
  ${whereClause}
  GROUP BY id`,
    queryParams,
  };
}
