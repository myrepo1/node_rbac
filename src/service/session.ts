import { Pool, PoolClient } from 'pg';
import pool from '../config/pool';
import { AccountCacheData } from '../type';
import { getAccountWithRoleConfigQuery } from './account';

/**
 * Add a new session to the database.
 *
 * @param {number} accountId - The ID of the account.
 * @param {AccountCacheData} config - The configuration data for the account.
 * @return {Promise<{ id: string }>} The ID of the newly created session.
 */
export async function addSession(params: {
  accountId: number;
  config: AccountCacheData;
}): Promise<{ id: string }> {
  const addSessionQuery = `
        INSERT INTO session(account_id,role_config,active)
        VALUES($1,$2,TRUE)
        RETURNING id
    `;
  const addSessionQueryResult = await pool.query(addSessionQuery, [
    params.accountId,
    params.config,
  ]);
  return addSessionQueryResult.rows[0] as { id: string };
}

/**
 * Get an active session.
 * @param accountId
 * @param sessionId
 * @returns
 */
export async function getSession(accountId: number, sessionId: string) {
  const getSessionQuery = `
        SELECT id,role_config
        FROM session
        WHERE id = $1 AND active = TRUE AND account_id = $2
    `;
  const getSessionQueryResult = await pool.query<{
    id: string;
    role_config: AccountCacheData;
  }>(getSessionQuery, [sessionId, accountId]);
  return getSessionQueryResult.rows[0];
}

/**
 * Stop single session
 * @param accountId
 * @param sessionId
 */
export async function stopSession(accountId: number, sessionId: string) {
  const stopSessionQuery = `
        UPDATE session
        SET active = FALSE
        WHERE id = $1 AND account_id = $2
    `;
  await pool.query(stopSessionQuery, [sessionId, accountId]);
}

/**
 * Stop all active session for certain account
 * @param accountId
 */
export async function stopAllSession(accountId: number) {
  const stopAllSessionQuery = `
        UPDATE session
        SET active = FALSE
        WHERE account_id = $1 AND active = TRUE
    `;
  await pool.query(stopAllSessionQuery, [accountId]);
}

/**
 * Update all active session for certain account
 * using recent account config
 * @param accountId
 * @param pool
 * @returns
 */
export async function updateSessionConfig(
  accountId: number,
  pool: Pool | PoolClient
) {
  try {
    const { query: accountConfigQuery, queryParams } =
      getAccountWithRoleConfigQuery({ id: accountId });
    const updateQuery = `
        WITH account_config AS (
            ${accountConfigQuery}
        )
        UPDATE session
        SET role_config = jsonb_build_object('roleIds',(SELECT role_ids FROM account_config),'permissionRestrictions',(SELECT permission_restrictions FROM account_config))
        WHERE account_id = $1 AND active = TRUE
    `;
    await pool.query(updateQuery, queryParams);
    return null;
  } catch (error) {
    return (error as Error).message;
  }
}
