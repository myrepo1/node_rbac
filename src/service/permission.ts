import * as z from 'zod';

import pool from '../config/pool';
import {
  AccessName,
  IContext,
  PermissionItem,
  PermissionName,
  PermissionSchema,
} from '../type';
import { getRoleCache, setRoleData } from './cache';

/**
 * Permission configuration for each access
 */
export const permissionSchema: PermissionSchema = {
  create_account: {
    operator: 'or',
    items: [
      { operator: 'and', items: ['create_account', 'attach_role_to_account'] },
      'create_account',
    ],
  },
  list_account: {
    operator: 'and',
    items: ['list_account'],
  },
  view_account: {
    operator: 'or',
    items: ['get_account_detail', 'restricted_get_account_detail'],
  },
  update_account: {
    operator: 'or',
    items: [
      {
        operator: 'and',
        items: [
          'update_account',
          'attach_role_to_account',
          'detach_role_from_account',
        ],
      },
      { operator: 'and', items: ['update_account', 'attach_role_to_account'] },
      {
        operator: 'and',
        items: ['update_account', 'detach_role_from_account'],
      },
      'update_account',
      'restricted_update_account',
    ],
  },
  delete_account: {
    operator: 'or',
    items: ['delete_account', 'restricted_delete_account'],
  },
  list_role: {
    operator: 'and',
    items: ['list_role'],
  },
  view_role: {
    operator: 'and',
    items: ['get_role_detail'],
  },
  create_role: {
    operator: 'or',
    items: [
      { operator: 'and', items: ['create_role', 'attach_permission_to_role'] },
      'create_role',
    ],
  },
  update_role: {
    operator: 'or',
    items: [
      {
        operator: 'and',
        items: [
          'update_role',
          'attach_permission_to_role',
          'detach_permission_from_role',
        ],
      },
      { operator: 'and', items: ['update_role', 'attach_permission_to_role'] },
      {
        operator: 'and',
        items: ['update_role', 'detach_permission_from_role'],
      },
      'update_role',
    ],
  },
  delete_role: {
    operator: 'and',
    items: ['delete_role'],
  },
  attach_permission_to_role: {
    operator: 'and',
    items: ['attach_permission_to_role'],
  },
  detach_permission_from_role: {
    operator: 'and',
    items: ['detach_permission_from_role'],
  },
  list_permissions: {
    operator: 'and',
    items: ['list_permissions'],
  },
};

/**
 * Validators for restricted access
 */
export const restrictedPermissionValidators = {
  restricted_get_account_detail: z.object({ ids: z.array(z.number().min(1)) }),
  restricted_update_account: z.object({ ids: z.array(z.number().min(1)) }),
  restricted_delete_account: z.object({ ids: z.array(z.number().min(1)) }),
} as const;

/**
 * Validate current context has access to accessName
 * @param accessName
 * @param context
 * @returns
 */
export function validateAccess(
  accessName: AccessName,
  context: IContext
): boolean {
  const permission = permissionSchema[accessName];
  if (!permission || !context.accountId) return false;
  const roles = context.roleIds;
  const rolePermissions: Set<PermissionName> = new Set();
  const roleData = getRoleCache();
  roles.forEach((role) => {
    roleData[role]?.forEach((permission) => {
      rolePermissions.add(permission);
    });
  });
  const result = validatePermission(
    permission,
    Array.from(rolePermissions),
    context.matchedPermissions
  );
  context.matchedPermissions = Array.from(new Set(context.matchedPermissions));
  return result;
}

/**
 * Recursive function to validate a permission item based on .
 * Incoming permissions will be compared with permission item,
 * and matched permissions will be stored in the current recursion.
 * @param {PermissionItem} permission - The permission item to validate.
 * @param {string[]} incomingPermissions - Array of roles.
 * @param {string[]} matchedPermissions - Array to store matched permissions.
 * @return {boolean} - True if the permission item is valid, false otherwise.
 */
function validatePermission(
  permission: PermissionItem,
  incomingPermissions: PermissionName[],
  matchedPermissions: PermissionName[]
): boolean {
  // Array to store matched permissions in the current recursion
  const currentMatchPermission: PermissionName[] = [];

  // Flag to determine if the permission item is valid
  let validateResult = false;

  // If the operator is 'and', validate that all items are valid
  if (permission.operator === 'and') {
    validateResult = permission.items.every((item) => {
      // If the item is a string, check if it's a valid role
      if (typeof item === 'string') {
        const incomingPermission = incomingPermissions.find((i) => i === item);
        // If the role is not valid, return false
        if (!incomingPermission) return false;
        // Push the matched permission to the current recursion
        currentMatchPermission.push(item);
        return true;
      }
      // If the item is an object, recursively validate it
      return validatePermission(
        item,
        incomingPermissions,
        currentMatchPermission
      );
    });
  }
  // If the operator is 'or', validate that at least one item is valid
  else if (permission.operator === 'or') {
    validateResult = permission.items.some((item) => {
      // If the item is a string, check if it's a valid role
      if (typeof item === 'string') {
        const incomingPermission = incomingPermissions.find((i) => i === item);
        // If the role is not valid, return false
        if (!incomingPermission) return false;
        // Push the matched permission to the current recursion
        currentMatchPermission.push(item);
        return true;
      }
      // If the item is an object, recursively validate it
      return validatePermission(
        item,
        incomingPermissions,
        currentMatchPermission
      );
    });
  }
  // If the permission item is valid, push the matched permissions to the input array and return true
  if (validateResult) {
    matchedPermissions.push(...currentMatchPermission);
    return true;
  }
  // If the permission item is not valid, return false
  return false;
}

/**
 * @deprecated
 */
export async function loadRoleData() {
  const roleQuery = `
        SELECT name,json_agg(permission_name) AS permissions
        FROM role
        JOIN permission_role ON role.id = permission_role.role_id
        GROUP BY name`;

  const roles = await pool.query<{ name: string; permissions: string[] }>(
    roleQuery
  );
  if (roles.rows.length === 0) return;
  setRoleData(
    roles.rows.reduce(
      (acc, { name, permissions }) => ({ ...acc, [name]: permissions }),
      {}
    )
  );
}

/**
 * This function is used to cache role data with its permissions. It takes an array of role IDs as
 * input and queries the database for the corresponding role data. The
 * function then updates the role cache with the retrieved data.
 *
 * @param roleIds - An array of role IDs for which the role data needs to be
 * cached.
 */
export async function cacheRole(roleIds: number[]) {
  const roleQuery = `
        SELECT role.id AS role_id,json_agg(permission_name) AS permissions
        FROM role
        JOIN role_permission ON role.id = role_permission.role_id
        WHERE role.id = any($1)
        GROUP BY role.id`;

  const roleQueryResult = await pool.query<{
    role_id: number;
    permissions: PermissionName[];
  }>(roleQuery, [roleIds]);

  if (roleQueryResult.rows.length === 0) return;

  const roleData = getRoleCache();

  roleQueryResult.rows.forEach((role) => {
    roleData[role.role_id] = role.permissions;
  });
}

export function updateAttachedCacheRole(
  roleId: number,
  permissions: PermissionName[]
) {
  if (!checkCacheRoleExist(roleId)) return;
  const roleData = getRoleCache();
  roleData[roleId] = Array.from(new Set([...roleData[roleId], ...permissions]));
}

export function detachCacheRole(roleId: number, permissions: string[]) {
  if (!checkCacheRoleExist(roleId)) return;
  const roleData = getRoleCache();
  roleData[roleId] = roleData[roleId].filter(
    (permission) => !permissions.includes(permission)
  );
}

export function unsetRole(roleId: number) {
  const roleData = getRoleCache();
  delete roleData[roleId];
}

/**
 * This function is used to check if the role cache exists.
 * @param roleId
 * @returns
 */
export function checkCacheRoleExist(roleId: number) {
  const roleData = getRoleCache();
  return !!roleData[roleId]?.length;
}

/**
 * This function is used to clear the role cache. It removes all entries from the role cache.
 */
export function clearCacheRole() {
  setRoleData({});
}

export function getCachedRolePermissions(roleId: number) {
  const roleData = getRoleCache();
  return [...(roleData[roleId] || [])];
}
