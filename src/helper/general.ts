export function getIncrFunc(initial = 0) {
  let value = initial;
  return () => {
    value += 1;
    return value;
  };
}

// Create delay function with args in ms
export function delay(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
