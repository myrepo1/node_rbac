import { faker } from '@faker-js/faker';

import pool from '../config/pool';
import { IAccount, PermissionName } from '../type';
import { getIncrFunc } from './general';
import { getRoleCache, getSingleAccountCache, setSingleAccountData } from '../service/cache';

/**
 * Generate dummy account for testing purpose
 * @param email
 * @param password
 * @returns
 */
export async function generateAccount(
  email = faker.internet.email(),
  password = faker.internet.password()
) {
  const accountQuery = `
        INSERT INTO account (email, name, password)
        VALUES ($1, $2, crypt($3, gen_salt('bf')))
        RETURNING id,email,name
    `;
  const accountQueryResult = await pool.query<IAccount>(accountQuery, [
    email,
    faker.person.fullName(),
    password,
  ]);
  return {
    id: accountQueryResult.rows[0].id,
    email,
    password,
    name: accountQueryResult.rows[0].name,
  };
}

/**
 * Generate dummy role for testing purpose
 * @param name
 * @param permissions
 * @returns
 */
export async function generateRole(
  name: string,
  permissions?: PermissionName[]
) {
  if (!permissions?.length) {
    const roleOnlyQuery = `INSERT INTO role (name,description) VALUES ($1,$2) RETURNING id`;
    const roleOnlyQueryResult = await pool.query<{ id: number }>(
      roleOnlyQuery,
      [name, faker.lorem.sentence()]
    );
    return roleOnlyQueryResult.rows[0].id;
  }
  const incrFunc = getIncrFunc(2);
  const rolePermissionClauses: string[] = [];
  const queryParams = [name, faker.lorem.sentence()];
  permissions.forEach((permission) => {
    rolePermissionClauses.push(`((SELECT id FROM role),$${incrFunc()})`);
    queryParams.push(permission);
  });
  const roleQuery = `
    WITH role AS (
      INSERT INTO role (name,description) VALUES ($1,$2) RETURNING id
    )
  INSERT INTO role_permission (role_id,permission_name) VALUES 
  ${rolePermissionClauses.join(',')}
  RETURNING role_id`;

  const roleQueryResult = await pool.query<{ role_id: number }>(
    roleQuery,
    queryParams
  );
  return roleQueryResult.rows[0].role_id;
}

/**
 * Attach multiple roles to an account and cache it
 * @param accountId
 * @param roleIds
 */
export async function attachRolesToAccount(
  accountId: number,
  roleIds: number[]
) {
  const incrFunc = getIncrFunc(1);
  const queryParams: number[] = [accountId];
  const insertValueClause: string[] = [];
  roleIds.forEach((roleId) => {
    insertValueClause.push(`($1,$${incrFunc()})`);
    queryParams.push(roleId);
  });
  const attachAccountQuery = `
    INSERT INTO account_role (account_id, role_id) VALUES
    ${insertValueClause.join(',')}
  `;
  await pool.query(attachAccountQuery, queryParams);
  const currentAccountCache = getSingleAccountCache(accountId);
  setSingleAccountData(accountId,{roleIds:Array.from(new Set([...(currentAccountCache?.roleIds||[]), ...roleIds]))})
}

/**
 * Clear all account, for clean up after testing
 */
export async function clearAccount() {
  const accountQuery = `DELETE FROM account`;
  await pool.query(accountQuery);
}

/**
 * Clear all role except role with name admin, for clean up after testing
 */
export async function clearRole() {
  const roleQuery = `DELETE FROM role WHERE name != 'admin'`;
  await pool.query(roleQuery);
}

/**
 * Clean up all test
 */
export async function cleanUpAllTest() {
  await pool.end();
}

<<<<<<< Updated upstream
export function getBaseContext<T = unknown>(): IContext<T> {
  return {
    accountId: 0,
    sessionId: '',
    roleIds: [],
    matchedPermissions: [],
    hasRestriction: false,
    restriction: null,
    sessionRestrictions: {},
  };
}

=======
/**
 * Cache role permissions without save to db
 * @param roleId 
 * @param permissions 
 */
>>>>>>> Stashed changes
export function cacheRoleWithoutDb(
  roleId: number,
  permissions: PermissionName[]
) {
  const roleData = getRoleCache();
  roleData[roleId] = [...permissions];
}

<<<<<<< Updated upstream
export function cacheAccountWithoutDb(accountId: number, roleId: number[]) {
  const cacheAccount = getAccountCache();
  cacheAccount[accountId] = {
    roleIds: [...roleId],
    permissionRestrictions:
      cacheAccount[accountId]?.permissionRestrictions || {},
  };
}

=======
>>>>>>> Stashed changes
/**
 * Set restriction for certain permission in a single attached role
 * @param accountId
 * @param roleId
 * @param permission
 * @param restriction
 */
export async function setRolePermissionRestriction(
  accountId: number,
  roleId: number,
  permission: PermissionName,
  restriction: unknown
) {
  const query = `
    WITH account_role AS (
      SELECT id
      FROM account_role
      WHERE account_id = $1 AND role_id = $2
    ), role_permission AS (
      SELECT id
      FROM role_permission
      WHERE role_id = $2 AND permission_name = $3
    )
    INSERT INTO role_restriction(account_role_id,role_permission_id,permission_restriction)
    VALUES ((SELECT id FROM account_role),(SELECT id FROM role_permission),$4)
  `;
  await pool.query(query, [accountId, roleId, permission, restriction]);
}
