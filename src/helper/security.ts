import crypto from 'crypto';
import jwt from 'jsonwebtoken';
import env from '../config/env';
import { CustomJWTPayload, IRole } from '../type';

export const salt = crypto.randomBytes(16).toString('hex');

export function generateJWT(
  payload: Record<string, unknown>,
  expiresIn: string,
  secret: string | null = null
) {
  return jwt.sign(payload, secret || env.jwtSecret, { expiresIn });
}

export function generateDummyAccessToken(
  type: 'accessToken' | 'refreshToken' = 'accessToken',
  id = 1,
  roles: IRole[] = [{ id: 1, name: 'admin' }],
  secret: string | null = null
) {
  return generateJWT({ id, type, roles }, '1h', secret);
}

/**
 * Validate a JWT token.
 * Expected JWT has no prefix "Bearer"
 * @param token
 * @returns
 */
export function validateJWT(token: string) {
  return jwt.verify(token, env.jwtSecret) as CustomJWTPayload;
}
