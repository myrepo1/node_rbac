import express, { Application, Request, Response } from 'express';
import getApp from './app';

// Boot express
const app = getApp();
const port = 5000;

// Start server
app.listen(port, () => console.log(`Server is listening on port ${port}!`));
