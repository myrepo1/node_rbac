import express, { Application } from 'express';
import dotenv from 'dotenv';
import accountRouter from './router/account';
import roleRouter from './router/role';

/**
 * Boot express application
 * @returns
 */
function getApp() {
  // Boot express
  dotenv.config();
  const app: Application = express();
  app.use(express.json());

  // Application routing
  app.use('/account', accountRouter);
  app.use('/role', roleRouter);
  app.use('/health', async (req, res) => {
    res.send('OK');
  });
  return app;
}

export default getApp;
