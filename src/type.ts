import { Request } from 'express';
import { JwtPayload } from 'jsonwebtoken';

export interface SuccessProcessResponse<T = null, U = null> {
  result: true;
  data: T;
  count?: U;
}

export interface FailedProcessResponse<> {
  result: false;
  message: string;
}

export type ProcessResponse<T = null, U = null> =
  | SuccessProcessResponse<T, U>
  | FailedProcessResponse;

export interface ISingleAccount {
  id: number;
  name: string;
  email: string;
  roles: {
    role: string;
    permissions: string[];
  }[];
}

export interface IAccount {
  id: number;
  email: string;
  name: string;
}

export type CustomJWTPayload = JwtPayload & {
  type: 'accessToken' | 'refreshToken';
  id: number;
  sessionId: string;
  roles?: IRole[];
};

export type PermissionName =
  | 'create_role'
  | 'update_role'
  | 'attach_permission_to_role'
  | 'detach_permission_from_role'
  | 'delete_role'
  | 'create_account'
  | 'list_account'
  | 'get_account_detail'
  | 'restricted_get_account_detail'
  | 'update_account'
  | 'restricted_update_account'
  | 'delete_account'
  | 'restricted_delete_account'
  | 'list_permissions'
  | 'list_role'
  | 'get_role_detail'
  | 'attach_role_to_account'
  | 'detach_role_from_account';

export type AccessName =
  | 'create_account'
  | 'list_account'
  | 'view_account'
  | 'update_account'
  | 'delete_account'
  | 'list_role'
  | 'view_role'
  | 'create_role'
  | 'update_role'
  | 'delete_role'
  | 'attach_permission_to_role'
  | 'detach_permission_from_role'
  | 'list_permissions';

export interface PermissionItem {
  operator: 'and' | 'or';
  items: (PermissionName | PermissionItem)[];
}

export type PermissionSchema = {
  [key in AccessName]: PermissionItem;
};

export interface IRole {
  id: number;
  name: string;
}

export interface IRoleDetail extends IRole {
  description?: string | null;
  permissions: string[];
}

export type IContext<T = unknown> = {
  accountId: number;
  sessionId: string;
  roleIds: number[];
  sessionRestrictions: Record<string, unknown>;
  matchedPermissions: PermissionName[];
} & (
  | { hasRestriction: true; restriction: T }
  | { hasRestriction: false; restriction: null }
);

export type CustomRequest = Request & { context?: IContext };

export interface CacheRoleData {
  [key: number]: PermissionName[];
}

export interface AccountRoleConfig {
  id: number;
  role_restrictions: ({ permission: string; restriction: unknown } | null)[];
}

export interface AccountWithRoleConfig {
  id: number;
  role_ids: number[];
  permission_restrictions: Record<string, unknown>;
}

export interface AccountCacheData {
  roleIds: number[];
  permissionRestrictions: Record<string, unknown>;
}
