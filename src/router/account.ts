import { Request, Response, Router } from 'express';
import {
  login,
  refresh,
  createAccount,
  getAccounts,
  getSingleAccount,
  updateAccount,
  deleteAccount,
  logOut,
} from '../service/account';
import { authMiddleware } from '../middleware/jwt';
import { getPermissionMiddleWare } from '../middleware/permission';
import { CustomRequest, IContext } from '../type';
import { attachContextMiddleware } from '../middleware/context';

const accountRouter = Router();

accountRouter.post('/login', async (req: Request, res: Response) => {
  const { email, password } = req.body;
  const loginResult = await login({ email, password });
  return res.status(loginResult.result ? 200 : 400).json(loginResult);
});

accountRouter.post('/refresh', async (req: Request, res: Response) => {
  const { refreshToken } = req.body;
  const result = await refresh(refreshToken);
  return res.status(result.result ? 200 : 400).json(result);
});

accountRouter.post(
  '/logout',
  [attachContextMiddleware, authMiddleware],
  async (req: CustomRequest, res: Response) => {
    const { accountId, sessionId } = req.context as IContext;
    const result = await logOut(accountId, sessionId);
    return res.status(result.result ? 200 : 400).json(result);
  }
);

accountRouter.post(
  '/create',
  [
    attachContextMiddleware,
    authMiddleware,
    getPermissionMiddleWare('create_account'),
  ],
  async (req: CustomRequest, res: Response) => {
    const createAccountResult = await createAccount(
      req.body,
      req.context as IContext
    );
    return res
      .status(createAccountResult.result ? 201 : 400)
      .json(createAccountResult);
  }
);

accountRouter.get(
  '/account',
  [
    attachContextMiddleware,
    authMiddleware,
    getPermissionMiddleWare('list_account'),
  ],
  async (req: Request, res: Response) => {
    const result = await getAccounts();
    return res.status(result.result ? 200 : 400).json(result);
  }
);

accountRouter.get(
  '/account/:id',
  [
    attachContextMiddleware,
    authMiddleware,
    getPermissionMiddleWare('view_account'),
  ],
  async (req: CustomRequest, res: Response) => {
    const { id } = req.params;
    const result = await getSingleAccount(Number(id), req.context as IContext);
    return res.status(result.result ? 200 : 404).json(result);
  }
);

accountRouter.put(
  '/account/:id',
  [
    attachContextMiddleware,
    authMiddleware,
    getPermissionMiddleWare('update_account'),
  ],
  async (req: CustomRequest, res: Response) => {
    const { id } = req.params;
    const result = await updateAccount(
      { ...(req.body || {}), id: Number(id) },
      req.context as IContext<{ ids: number[] }>
    );
    return res.status(result.result ? 200 : 400).json(result);
  }
);

accountRouter.delete(
  '/account/:id',
  [
    attachContextMiddleware,
    authMiddleware,
    getPermissionMiddleWare('delete_account'),
  ],
  async (req: CustomRequest, res: Response) => {
    const result = await deleteAccount(
      Number(req.params.id),
      req.context as IContext
    );
    return res.status(result.result ? 200 : 400).json(result);
  }
);

export default accountRouter;
