import { Request, Response, Router } from 'express';
import {
  createRole,
  deleteRole,
  getDetailRole,
  listRoles,
  updateRole,
} from '../service/role';
import { attachContextMiddleware } from '../middleware/context';
import { authMiddleware } from '../middleware/jwt';
import { getPermissionMiddleWare } from '../middleware/permission';
import { CustomRequest, IContext } from '../type';

const roleRouter = Router();

roleRouter.get(
  '/role',
  [
    attachContextMiddleware,
    authMiddleware,
    getPermissionMiddleWare('list_role'),
  ],
  async (req: Request, res: Response) => {
    const roles = await listRoles();
    return res.status(roles.result ? 200 : 400).json(roles);
  }
);

roleRouter.post(
  '/role',
  [
    attachContextMiddleware,
    authMiddleware,
    getPermissionMiddleWare('create_role'),
  ],
  async (req: CustomRequest, res: Response) => {
    const { name, description, permissions } = req.body;
    const roles = await createRole(
      { name, description, permissions },
      req.context as IContext
    );
    return res.status(roles.result ? 201 : 400).json(roles);
  }
);

roleRouter.get(
  '/role/:id',
  [
    attachContextMiddleware,
    authMiddleware,
    getPermissionMiddleWare('view_role'),
  ],
  async (req: CustomRequest, res: Response) => {
    const roles = await getDetailRole(Number(req.params.id));
    return res.status(roles.result ? 200 : 404).json(roles);
  }
);

roleRouter.put(
  '/role/:id',
  [
    attachContextMiddleware,
    authMiddleware,
    getPermissionMiddleWare('update_role'),
  ],
  async (req: CustomRequest, res: Response) => {
    const roles = await updateRole(
      { ...req.body, id: Number(req.params.id) },
      req.context as IContext
    );
    return res.status(roles.result ? 200 : 400).json(roles);
  }
);

roleRouter.delete(
  '/role/:id',
  [
    attachContextMiddleware,
    authMiddleware,
    getPermissionMiddleWare('delete_role'),
  ],
  async (req: CustomRequest, res: Response) => {
    const roles = await deleteRole(Number(req.params.id));
    return res.status(roles.result ? 200 : 400).json(roles);
  }
);

export default roleRouter;
